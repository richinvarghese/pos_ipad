//
//  RMGlobalManager.swift
//  StreetWall
//
//  Created by Shebin Koshy on 03/09/17.
//  Copyright © 2017 Shebin Koshy. All rights reserved.
//

import UIKit

class PSCommon: NSObject {
    
    struct Global {
  //  static let baseURL : String = "http://demo-pos.akstech.com.sg/api/"
//static let thumbnail : String = "http://demo-pos.akstech.com.sg/assets/upload/products/xsmall/"
    //    static let baseURL : String = "http://test.apstrix.com/api/"
      // static let thumbnail : String = "http://test.apstrix.com/assets/upload/products/xsmall/"
        
   /* static let baseURL : String = "http://4esbyrita.akstech.com.sg/api/"
    static let thumbnail : String = "http://4esbyrita.akstech.com.sg/assets/upload/products/xsmall/"
    static let base : String = "http://4esbyrita.akstech.com.sg/"
 */
       static let baseURL : String = "http://srijumbo.akstech.com.sg/api/"
    static let thumbnail : String = "http://srijumbo.akstech.com.sg/assets/upload/products/xsmall/"
    static let base : String = "http://srijumbo.akstech.com.sg/"
 

   /*static let baseURL : String = "http://localhost/apstrix/pos/api/"
      static let thumbnail : String = "http://localhost/apstrix/pos/assets/upload/products/xsmall/"
       static let base : String = "http://localhost/apstrix/pos/"
 */
        static  let authToken: String = ""
    }
    
    /* public class func appBlueColor() -> UIColor {
     return UIColor(red:85, green: 182, blue:236)//#55B6EC
     //        return UIColor(red: 21, green: 38, blue: 49)
     }*/
    
    public class func appName() -> String {
        let info = Bundle.main.infoDictionary!
        return info[kCFBundleNameKey! as String] as! String
    }
    
    public class func appVersion() -> String {
        guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return "no version info"
        }
        return version
    }
    
    public class func defaultRadiusForFetchNearbyEvents() -> Int {
        return 5
    }
    
    
    class func pickerView(textField:UITextField,toolBarButtonTitle:String, pickerViewDelegateAndDataSource:Any?, barButtonTarget: Any, barButtonAction:Selector) -> UIPickerView {
        
        let (pickerView, toolbarWithBarButton) = pickerViewAndToolbar(toolBarButtonTitle: toolBarButtonTitle, pickerViewDelegateAndDataSource: pickerViewDelegateAndDataSource, barButtonTarget: barButtonTarget, barButtonAction: barButtonAction)
        
        textField.inputView = pickerView
        
        textField.inputAccessoryView = toolbarWithBarButton
        return pickerView
    }
    
    private class func pickerViewAndToolbar(toolBarButtonTitle:String, pickerViewDelegateAndDataSource:Any?, barButtonTarget: Any, barButtonAction:Selector) -> (UIPickerView,UIToolbar) {
        let pickerView = UIPickerView.init(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        pickerView.delegate = pickerViewDelegateAndDataSource as? UIPickerViewDelegate
        pickerView.dataSource = pickerViewDelegateAndDataSource as? UIPickerViewDataSource
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        let barButtonItem = UIBarButtonItem(title: toolBarButtonTitle, style: .done, target: barButtonTarget, action: barButtonAction)
        
        let toolbarWithBarButton = self.toolbar(barButtonItem: barButtonItem)
        return (pickerView,toolbarWithBarButton)
    }
    
    class func toolbar(barButtonItem: UIBarButtonItem) -> UIToolbar {
        
        let toolbar = UIToolbar()
        
        toolbar.barStyle = .default
        toolbar.frame = CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:40)
        
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),barButtonItem]
        return toolbar
    }
    
    public class func dateFromString(stringOnlyDate:String) -> Date{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        //        dateformatter.dateFormat = "MM/dd/yy"
        
        let date = dateFormatter.date(from: stringOnlyDate)
        return date!
    }
    
    
    public class func shortDateAndTimeString(date:Date) -> String{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()
        //        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        //        dateformatter.dateFormat = "MM/dd/yy"
        return dateFormatter.string(from: date)
        //        let date = dateFormatter.date(from: stringOnlyDate)
        //        return date!
    }
    
    
    public class func dateFromWebServiceString(stringDate:String) -> Date?{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()//2017-12-14 15:04:55
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: stringDate)
        return date
    }
    
    //    public class func dateFromWebServiceString(stringDateAndTime:String) -> Date?{
    //        //        let dateformatter = DateFormatter()
    //
    //        let dateFormatter = DateFormatter()//2017-12-14 15:04:55
    ////        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    ////        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    //        let date = dateFormatter.date(from: stringDateAndTime)
    //        return date
    //    }
    
    
    public class func dateFromWebServiceString(stringOnlyDate:String) -> Date?{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()//2017-12-14 15:04:55
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //        dateFormatter.timeStyle = .none
        let date = dateFormatter.date(from: stringOnlyDate)
        return date!
    }
    
    
    public class func webServiceOnlyStringDateFromDate(date:Date) -> String{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()//2017-12-14 15:04:55
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //        dateFormatter.timeStyle = .none
        let stringDate = dateFormatter.string(from: date)
        return stringDate
        //        let date = dateFormatter.date(from: stringOnlyDate)
        //        return date!
    }
    
    public class func webServiceStringDateAndTimeFromDate(date:Date) -> String{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()//2017-12-14 15:04:55
        //        dateFormatter.dateFormat = "yyyy-MM-dd "
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //        dateFormatter.timeStyle = .none
        let stringDate = dateFormatter.string(from: date)
        return stringDate
        //        let date = dateFormatter.date(from: stringOnlyDate)
        //        return date!
    }
    
    
    public class func utcDate(date:Date) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let string = dateFormatter.string(from: date)
        return dateFormatter.date(from: string)!
    }
    
    public class func utcDateRemovingTime(date:Date) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeStyle = .none
        let string = dateFormatter.string(from: date)
        return dateFormatter.date(from: string)!
    }
    
    
    
    public class func onlyDateStringFromDate(date:Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        
        //        print(dateFormatter.string(from: date))
        return dateFormatter.string(from: date)
    }
    
    public class func timeStringFromDate(date:Date) -> String {
        let calendar = Calendar.current
        
        var hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        print("hours = \(hour):\(minutes):\(seconds)")
        var amOrpm = "AM"
        if hour > 12 {
            hour = hour - 12
            amOrpm = "PM"
        }
        if hour == 0 {
            hour = 12
        }
        return ("\(String(format: "%02d", hour)):\(String(format: "%02d", minutes)) \(amOrpm)")
    }
    
    public class func deviceTimeZoneAbbreviation() -> String? {
        return TimeZone.current.abbreviation()
    }
    
    public class func deviceLanguage() -> String? {
        let languageCode = NSLocale.current.languageCode
        let regionCode = NSLocale.current.regionCode
        var language : String?
        if languageCode != nil && regionCode != nil {
            language = "\(languageCode!)-\(regionCode!)"
        } else if languageCode != nil {
            language = languageCode
        } else if regionCode != nil {
            language = regionCode
        }
        return language
    }
    
    public class func oldDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.year = 2017
        dateComponents.month = 10
        dateComponents.day = 1
        
        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponents)
        return someDateTime!
    }
    
    
    public class func yesterdayDate() -> Date {
        let yesterdayDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        //        let threeDaysAfterDate = Calendar.current.date(byAdding: .day, value: 3, to: Date())//NOT CORRECT. its for only testing purpose
        return yesterdayDate!
    }
    
    public class func tomorrowDate() -> Date {
        let tomorrowDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        //        let threeDaysAfterDate = Calendar.current.date(byAdding: .day, value: 3, to: Date())//NOT CORRECT. its for only testing purpose
        return tomorrowDate!
    }
    
    public class func threeDaysBackDate() -> Date {
        let threeDaysBackDate = Calendar.current.date(byAdding: .day, value: -3, to: Date())
        //        let threeDaysAfterDate = Calendar.current.date(byAdding: .day, value: 3, to: Date())//NOT CORRECT. its for only testing purpose
        return threeDaysBackDate!
    }
    
    
    private class func dayDiffrenceWithCurrentDate(pastDate:Date) -> Int {
        let currentDate = Date()
        let dayDifference = Calendar.current.dateComponents([.day], from: pastDate, to: currentDate).day ?? 0
        return dayDifference
    }
    
    
    
    public class func addPlaceholderForView(view:UIView, text:String, font: UIFont) -> UILabel {
        let leftPadding = 8 as CGFloat
        let labelPlaceHolder = UILabel(frame: CGRect(x:leftPadding,y:CGFloat(0),width:view.frame.width,height:30))
        labelPlaceHolder.font = font
        labelPlaceHolder.textAlignment = .left
        labelPlaceHolder.text = text
        labelPlaceHolder.textColor = UIColor.lightGray
        view.addSubview(labelPlaceHolder)
        return labelPlaceHolder
    }
    
    
    
    public class func isDevelopment() -> Bool {
        return true
      //  return false
    }
    
    
    public class func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    
    public class func compressImage(image:UIImage) -> Data? {
        // Reducing file size to a 10th
        
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 1136.0
        let maxWidth : CGFloat = 640.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = UIImageJPEGRepresentation(img, compressionQuality)else{
            return nil
        }
        return imageData
    }
    
    
    
    
    
    deinit {
        print("RMGlobalManager deallocated")
    }
    
    
    
    
}
