import UIKit
import Alamofire
class PSAPIClient: NSObject {
    enum WebServiceNames: String {
         //case baseUrl  = "http://localhost/apstrix/pos/"
        case baseUrl = "http://srijumbo.akstech.com.sg/"
//  case baseUrl =  "http://4esbyrita.akstech.com.sg/"
       // case baseUrl = "http://ojeen.in/q8mount/api/"
        case Login   = "index.php/api/rest/signin"
        case signUp  = "index.php/api/rest/signup"
        case profile = "index.php/api/rest/profile"
        case editProfile = "index.php/api/rest/editprofile"
        case banner = "index.php/api/rest/banner"
        case featured = "index.php/api/rest/featured"
        case newArrival = "index.php/api/rest/newarrival"
        case productdetails = "index.php/api/rest/details"
        case category = "index.php/api/rest/category"
        case productcategory = "index.php/api/rest/productcategory"
        case address = "index.php/api/rest/address"
        case area = "index.php/api/rest/area"
        case country = "/index.php/api/rest/country"
        case updateAddress = "index.php/api/rest/updateaddress"
        case productById = "index.php/api/rest/productbyid"
        case addAddress = "index.php/api/rest/addaddress"
        // case removeAddress = "index.php/api/rest/address"
        case wishList = "index.php/api/rest/whisList"
        case checkOut = "index.php/api/rest/CheckOut"
        case search = "index.php/api/rest/search"
        case myOrder = "index.php/api/rest/myorder"
        case subCategory = "index.php/api/rest/subcategory"
        case Filter = "index.php/api/rest/filter"
        case Year = "index.php/api/rest/year"
        case makes = "index.php/api/rest/makes"
        case model = "index.php/api/rest/models"
        case subModel = "index.php/api/rest/submodel"
        case serachByAttr = "index.php/api/rest/searchbyattr"
        
        case salesListByStatus = "index.php/api/report/listbystatus"
        case voidOrder = "index.php/api/report/voidorder"

    }
    var loginDictionary  = NSDictionary()
    var signUpDictionary = NSDictionary()
    var profileDictionary = NSDictionary()
    var editProfileDictionary = NSDictionary()
    var bannerDictionary  = NSDictionary()
    var feauredDictionary = NSDictionary()
    var newArrivalDictionary = NSDictionary()
    var productDetailsDictionary = NSDictionary()
    var categoryDictionary = NSDictionary()
    var productCategoryDictionary = NSDictionary()
    var addressDictionary = NSDictionary()
    var areaDictionary = NSDictionary()
    var coutryDictionary = NSDictionary()
    var updateAddressDictionary = NSDictionary()
    var productByIdDictionary = NSDictionary()
    var addAddressDictionary = NSDictionary()
    var removeAddressDictionary = NSDictionary()
    var wishListDictionary = NSDictionary()
    var checkOutDictionary = NSDictionary()
    var searchDictionary = NSDictionary()
    var myOrderDictionary = NSDictionary()
    var subCategoryDictionary = NSDictionary()
    var yearDictionary = NSDictionary()
    var makesDictionary = NSDictionary()
    var modelDictionary = NSDictionary()
    var subModelDictionary = NSDictionary()
    var serachByAttrDictionary = NSDictionary()
    var getWishListDictionary = NSDictionary()
    var removeWishListDictionary = NSDictionary()
    var filterDictionary = NSDictionary()
    var transactionDictionary = NSDictionary()
     var voidDictionary = NSDictionary()
    static  let  authToken = ""
    
    // MARK :-login
    class func login( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.Login.rawValue
        let parameter = params
       
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .post, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.loginDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-signUp
    class func signUp( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.signUp.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .post, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.signUpDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-profileData
    class func profile( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.profile.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.profileDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-EditProfile
    class func editProfile( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.editProfile.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .post, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.editProfileDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-bannerDetails
    class func banner( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.banner.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.bannerDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-featuredProducts
    class func featuredProducts( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.featured.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.feauredDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-NewArrival
    class func NewArrival( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.newArrival.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.newArrivalDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-Productdetails
    class func Productdetails( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.productdetails.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.productDetailsDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-productcategory
    class func productcategory( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.productcategory.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.productCategoryDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-category
    class func category( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.category.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.categoryDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-getShippingAddress
    class func getShippingAddress( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.address.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.addressDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-getArea
    class func getArea( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.area.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.areaDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-getCountry
    class func getCountry( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.country.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.coutryDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-updateAddress
    class func updateAddress( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.updateAddress.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .post, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.updateAddressDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-productByID
    class func productByID( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.productById.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.productByIdDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    //    // MARK :-productByID
    //    class func productByID( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
    //        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.productById.rawValue
    //        let parameter = params
    //        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
    //            print(response ?? "Error")
    //            let result = PSAPIClient()
    //            if let data = response as? NSDictionary {
    //                result.productByIdDictionary = data
    //                completion(result, "Success", true)
    //            }else {
    //                completion("" as AnyObject?, "Failed", false)
    //            }
    //        }
    //    }
    // MARK :-AddNewAddress
    class func AddNewAddress( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.addAddress.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.addAddressDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-RemoveAddress
    class func RemoveAddress( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.address.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .delete, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.removeAddressDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-AddWishList
    class func AddWishList( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.wishList.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .post, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.wishListDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-GetWishList
    class func GetWishList( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.wishList.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.getWishListDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-Check Out
    class func CheckOut( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.checkOut.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.getWishListDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-Removefromwishlist
    class func Removefromwishlist( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.wishList.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .delete, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.removeWishListDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-Search
    class func Search( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.search.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.searchDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-Myorder
    class func Myorder( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.myOrder.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.myOrderDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :-Sub Category
    class func SubCategory( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.subCategory.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.subCategoryDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :- Filter
    class func filter( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.Filter.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.filterDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :- Year
    class func Year( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.Year.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.yearDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    //    // MARK :- Filter
    //    class func filter( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
    //        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.Filter.rawValue
    //        let parameter = params
    //        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
    //            print(response ?? "Error")
    //            let result = PSAPIClient()
    //            if let data = response as? NSDictionary {
    //                result.filterDictionary = data
    //                completion(result, "Success", true)
    //            }else {
    //                completion("" as AnyObject?, "Failed", false)
    //            }
    //        }
    //    }
    // MARK :- Makes
    class func makes( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.makes.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.makesDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    //    // MARK :- Makes
    //    class func makes( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
    //        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.makes.rawValue
    //        let parameter = params
    //        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
    //            print(response ?? "Error")
    //            let result = PSAPIClient()
    //            if let data = response as? NSDictionary {
    //                result.makesDictionary = data
    //                completion(result, "Success", true)
    //            }else {
    //                completion("" as AnyObject?, "Failed", false)
    //            }
    //        }
    //    }
    // MARK :- Model
    class func model( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.model.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.modelDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :- SubModel
    class func submodel( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.subModel.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.subModelDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    // MARK :- SearchByAttr
    class func SearchByAttr( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.serachByAttr.rawValue
        let parameter = params
        PSAPIClient.alamofireFunctionWithRegisterHeader(urlString: url, method: .get, parameters: parameter, token: authToken){ (response, message, status) in
            print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.subModelDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    //MARK :- SalesListByStatus
    class func SalesListByStatus( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.salesListByStatus.rawValue
        let parameter = params
        PSAPIClient.alamofireFunction(urlString: url, method: .post, paramters: parameter){ (response, message, status) in
          // print(response)
            //print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.transactionDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    
    
    //MARK :- SalesListByStatus
    class func VoidOrder( params:[String:AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> ()) {
        let url = WebServiceNames.baseUrl.rawValue+WebServiceNames.voidOrder.rawValue
        let parameter = params
        PSAPIClient.alamofireFunction(urlString: url, method: .post, paramters: parameter){ (response, message, status) in
            
             print(response)
            //print(response ?? "Error")
            let result = PSAPIClient()
            if let data = response as? NSDictionary {
                result.voidDictionary = data
                completion(result, "Success", true)
            }else {
                completion("" as AnyObject?, "Failed", false)
            }
        }
    }
    
    /****************************************/
    //MARK :- Get
    class func getWebService(urlString: String, params: [String : AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> Void) {
        alamofireFunction(urlString: urlString, method: .get, paramters: params) { (response, message, success) in
            if response != nil {
                completion(response as AnyObject?, "", true)
            }else{
                completion(nil, "", false)
            }
        }
    }
    
    //MARK :- Post
    class func postWebService(urlString: String, params: [String : AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> Void) {
        alamofireFunction(urlString: urlString, method: .post, paramters: params) { (response, message, success) in
            if response != nil {
                completion(response as AnyObject?, "", true)
            }else{
                completion(nil, "", false)
            }
        }
    }
    
    class func alamofireFunction(urlString : String, method : Alamofire.HTTPMethod, paramters : [String : AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            Alamofire.request(urlString, method: .post, parameters: paramters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                print(urlString)
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        } else {
            Alamofire.request(urlString).responseJSON { (response) in
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        }
    }
    
    //MARK :- Only for testing
    
    class func alamofireFunctionTesting(urlString : String, method : Alamofire.HTTPMethod, paramters : [String : AnyObject], completion : @escaping (_ response : AnyObject?, _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            Alamofire.request(urlString, method: .post, parameters: paramters, encoding: URLEncoding.default, headers: nil).responseString { (response:DataResponse<String>) in
                print("hoo")
                print(urlString)
                
                if response.result.isSuccess{
                      print("hoo1")
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                      print("hoo2")
                    completion(nil, "", false)
                }
            }
        } else {
            Alamofire.request(urlString).responseString { (response) in
                
                if response.result.isSuccess{
                      print("hoo3")
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                      print("hoo4")
                    completion(nil, "", false)
                }
            }
        }
    }
    
    
    
    //MARK :- PostWithHeader
    class func postWebServiceWithRegisterHeader(urlString: String, params: [String : AnyObject], AuthToken: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void) {
        alamofireFunctionWithRegisterHeader(urlString: urlString, method: .post, parameters: params, token: AuthToken) { (response, message, success) in
            if response != nil {
                completion(response as AnyObject?, "", true)
            } else{
                completion(nil, "", false)
            }
        }
    }
    
    class func postWebServiceWithTokenHeader(urlString: String, params: [String : AnyObject], urlToken: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void) {
        alamofireFunctionWithTokenHeader(urlString: urlString, method: .post, parameters: params, token: urlToken) { (response, message, success) in
            if response != nil {
                completion(response as AnyObject?, "", true)
            }else{
                completion(nil, "", false)
            }
        }
    }
    
    class func postWebServiceWithUserHeader(urlString: String, params: [String : AnyObject], urlToken: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void) {
        alamofireFunctionWithUserHeader(urlString: urlString, method: .post, parameters: params, token: urlToken) { (response, message, success) in
            if response != nil {
                completion(response as AnyObject?, "", true)
            }else{
                completion(nil, "", false)
            }
        }
    }
    
    //Location Header
    class func postWebServiceWithLocationHeader(urlString: String, params: [String : AnyObject], AuthToken: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void) {
        alamofireFunctionWithLocationHeader(urlString: urlString, method: .post, parameters: params, token: AuthToken) { (response, message, success) in
            if response != nil {
                completion(response as AnyObject?, "", true)
            }else{
                completion(nil, "", false)
            }
        }
    }
    class func alamofireFunctionWithRegisterHeaderjson(urlString : String, method : Alamofire.HTTPMethod, parameters : [String : AnyObject], token: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["Authorization" : token]).responseJSON { (response:DataResponse<Any>) in
                print(urlString)
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
            
        }
    }
    class func alamofireFunctionWithRegisterHeader(urlString : String, method : Alamofire.HTTPMethod, parameters : [String : AnyObject], token: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: ["Authorization" : token]).responseJSON { (response:DataResponse<Any>) in
                print(urlString)
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
            
        }else if method == Alamofire.HTTPMethod.get {
            Alamofire.request(urlString, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: ["Authorization" : token]).responseJSON { (response:DataResponse<Any>) in
                print(urlString)
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        }
        else{
            
            Alamofire.request(urlString).responseJSON { (response) in
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        }
        
    }
    
    class func alamofireFunctionWithTokenHeader(urlString : String, method : Alamofire.HTTPMethod, parameters : [String : AnyObject], token: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: ["token" : token]).responseJSON { (response:DataResponse<Any>) in
                print(urlString)
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
            
        }else {
            Alamofire.request(urlString).responseJSON { (response) in
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        }
    }
    
    class func alamofireFunctionWithUserHeader(urlString : String, method : Alamofire.HTTPMethod, parameters : [String : AnyObject], token: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: ["usrtoken" : token]).responseJSON { (response:DataResponse<Any>) in
                print(urlString)
                
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
            
        }else {
            Alamofire.request(urlString).responseJSON { (response) in
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        }
    }
    
    //Location Header
    class func alamofireFunctionWithLocationHeader(urlString : String, method : Alamofire.HTTPMethod, parameters : [String : AnyObject], token: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void){
        
        if method == Alamofire.HTTPMethod.post {
            //            Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: ["usrtoken" : token,"lat": latToken!,"lng":longToken!]).responseJSON { (response:DataResponse<Any>) in
            //                print(urlString)
            //
            //                if response.result.isSuccess{
            //                    completion(response.result.value as AnyObject?, "", true)
            //                }else{
            //                    completion(nil, "", false)
            //                }
            //            }
            //
        }else {
            Alamofire.request(urlString).responseJSON { (response) in
                if response.result.isSuccess{
                    completion(response.result.value as AnyObject?, "", true)
                }else{
                    completion(nil, "", false)
                }
            }
        }
    }
    
    //Image
    class func postWebServiceWithImage(urlString: String, params: [String : AnyObject], imageToUploadURL: UIImage,fileType:String,mimeName: String, token: String, completion : @escaping ( _ response : AnyObject?,  _ message: String?, _ success : Bool)-> Void) {
        let imageData = UIImageJPEGRepresentation(imageToUploadURL,1.0)
        let URL = try! URLRequest(url: urlString, method: .post, headers: ["Authorization" : token])
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData!, withName: "image", fileName: fileType, mimeType: mimeName)
            
            for (key, value) in params {
                // multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                //  multipartFormData.app
                //appendBodyPart(data: value.dataUsingEncoding(String.Encoding.utf8)!, name: key)
                multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
            }
        }, with: URL, encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        completion(response.result.value as AnyObject?, "", true)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completion(nil, "", false)
            }
        })
    }
    
    //Mark:-Cancel
    class func cancelAllRequests()
    {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
}
