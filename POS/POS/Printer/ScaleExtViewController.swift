//
//  ScaleExtViewController.swift
//  Swift SDK
//
//  Created by Yuji on 2016/**/**.
//  Copyright © 2016年 Star Micronics. All rights reserved.
//

import Foundation

class ScaleExtViewController: CommonViewController, UIAlertViewDelegate {
    enum ScaleStatus: Int {
        case invalid = 0
        case impossible
        case connect
        case disconnect
    }
    
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var zeroClearButton:  UIButton!
    @IBOutlet weak var unitChangeButton: UIButton!
    
    var port: SMPort!
    
    var scaleStatus: ScaleStatus!
    
    var lock: NSRecursiveLock!
    
    var dispatchGroup: DispatchGroup!
    
    var terminateTaskSemaphore: DispatchSemaphore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.commentLabel.text = ""
        
        self.commentLabel.adjustsFontSizeToFitWidth = true
        
        self.zeroClearButton.isEnabled           = true
//      self.zeroClearButton.backgroundColor   = UIColor.cyan
        self.zeroClearButton.backgroundColor   = UIColor(red: 0.8, green: 0.8, blue: 1.0, alpha: 1.0)
        self.zeroClearButton.layer.borderColor = UIColor.blue.cgColor
        self.zeroClearButton.layer.borderWidth = 1.0
        
        self.unitChangeButton.isEnabled           = true
        self.unitChangeButton.backgroundColor   = UIColor.cyan
        self.unitChangeButton.layer.borderColor = UIColor.blue.cgColor
        self.unitChangeButton.layer.borderWidth = 1.0
        
        self.appendRefreshButton(#selector(ScaleExtViewController.refreshScale))
        
        self.port = nil
        
        self.scaleStatus = ScaleStatus.invalid
        
        self.lock = NSRecursiveLock()
        
        self.dispatchGroup = DispatchGroup()
        
        self.terminateTaskSemaphore = DispatchSemaphore(value: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ScaleExtViewController.applicationWillResignActive), name: NSNotification.Name(rawValue: "UIApplicationWillResignActiveNotification"), object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ScaleExtViewController.applicationDidBecomeActive),  name: NSNotification.Name(rawValue: "UIApplicationDidBecomeActiveNotification"),  object:nil)
        
        GlobalQueueManager.shared.serialQueue.async {
            DispatchQueue.main.async {
                self.refreshScale()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        GlobalQueueManager.shared.serialQueue.async {
            _ = self.disconnect()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UIApplicationWillResignActiveNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UIApplicationDidBecomeActiveNotification"),  object: nil)
    }
    
    @objc func applicationDidBecomeActive() {
        self.beginAnimationCommantLabel()
        
        GlobalQueueManager.shared.serialQueue.async {
            DispatchQueue.main.async {
                self.refreshScale()
            }
        }
    }
    
    @objc func applicationWillResignActive() {
        GlobalQueueManager.shared.serialQueue.async {
            _ = self.disconnect()
        }
    }
    
    @IBAction func touchUpInsideZeroClearButton(_ sender: UIButton) {
        let builder: ISSCBBuilder = StarIoExt.createScaleCommandBuilder(StarIoExtScaleModel.APS20)
        
        ScaleFunctions.appendZeroClear(builder: builder)
        
//      let commands: Data = builder.commands           .copy() as! Data
        let commands: Data = builder.passThroughCommands.copy() as! Data
        
        self.blind = true
        
        self.lock.lock()
        
        if self.scaleStatus == ScaleStatus.connect {
            GlobalQueueManager.shared.serialQueue.async {
                _ = Communication.sendCommandsDoNotCheckCondition(commands, port: self.port, completionHandler: { (result: Bool, title: String, message: String) in
                    DispatchQueue.main.async {
                        if result == false {
                            let alertView: UIAlertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
                            
                            alertView.show()
                        }
                        
                        self.lock.unlock()
                        
                        self.blind = false
                    }
                })
            }
        }
        else {
            let alertView: UIAlertView = UIAlertView(title: "Failure", message: "Scale Disconnect.", delegate: nil, cancelButtonTitle: "OK")
            
            alertView.show()
            
            self.lock.unlock()
            
            self.blind = false
        }
    }
    
    @IBAction func touchUpInsideUnitChangeButton(_ sender: UIButton) {
        let builder: ISSCBBuilder = StarIoExt.createScaleCommandBuilder(StarIoExtScaleModel.APS20)
        
        ScaleFunctions.appendUnitChange(builder: builder)
        
//      let commands: Data = builder.commands           .copy() as! Data
        let commands: Data = builder.passThroughCommands.copy() as! Data
        
        self.blind = true
        
        self.lock.lock()
        
        if self.scaleStatus == ScaleStatus.connect {
            GlobalQueueManager.shared.serialQueue.async {
                _ = Communication.sendCommandsDoNotCheckCondition(commands, port: self.port, completionHandler: { (result: Bool, title: String, message: String) in
                    DispatchQueue.main.async {
                        if result == false {
                            let alertView: UIAlertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
                            
                            alertView.show()
                        }
                        
                        self.lock.unlock()
                        
                        self.blind = false
                    }
                })
            }
        }
        else {
            let alertView: UIAlertView = UIAlertView(title: "Failure", message: "Scale Disconnect.", delegate: nil, cancelButtonTitle: "OK")
            
            alertView.show()
            
            self.lock.unlock()
            
            self.blind = false
        }
    }
    
    @objc func refreshScale() {
        self.blind = true
        
        defer {
            self.blind = false
        }
        
        _ = self.disconnect()
        
        if self.connect() == false {
            let alertView: UIAlertView = UIAlertView(title: "Fail to Open Port.", message: "", delegate: self, cancelButtonTitle: "OK")
            
            alertView.show()
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        self.commentLabel.text = "Check the device. (Power and Bluetooth pairing)\nThen touch up the Refresh button."
        
        self.commentLabel.textColor = UIColor.red
        
        self.beginAnimationCommantLabel()
    }
    
    func connect() -> Bool {
        var result: Bool = true
        
        if self.port == nil {
            result = false
            
            while true {
                let portName = AppDelegate.getPortName()
                self.port = SMPort.getPort(portName, AppDelegate.getPortSettings(), 10000)     // 10000mS!!!
                
                if self.port == nil {
                    break
                }
                
                // Sleep to avoid a problem which sometimes cannot communicate with Bluetooth.
                // (Refer Readme for details)
                if #available(iOS 11.0, *), portName.uppercased().hasPrefix("BT:") {
                    Thread.sleep(forTimeInterval: 0.2)
                }
                
                var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
                
                var error: NSError?
                
                self.port.getParsedStatus(&printerStatus, 2, &error)
                
                if error != nil {
                    SMPort.release(self.port)
                    
                    self.port = nil
                    break
                }
                
                _ = self.terminateTaskSemaphore.wait(timeout: DispatchTime.distantFuture)
                
                DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(group: self.dispatchGroup) { () -> Void in
                    self.watchScaleTask()
                }
                
                result = true
                break
            }
        }
        
        return result
    }
    
    func disconnect() -> Bool {
        var result: Bool = true
        
        if self.port != nil {
            result = false
            
            self.terminateTaskSemaphore.signal()
            
            _ = self.dispatchGroup.wait(timeout: DispatchTime.distantFuture)
            
            SMPort.release(self.port)
            
            self.port = nil
            
            self.scaleStatus = ScaleStatus.invalid
            
            result = true
        }
        
        return result
    }
    
    func watchScaleTask() {
        var terminate: Bool = false
        
        while terminate == false {
            autoreleasepool {
//              var portValid = false
                var portValid = true
                
                if self.lock.try() {
                    portValid = false
                    
                    if self.port != nil {
                        if self.port.connected() == true {
                            portValid = true
                        }
                    }
                    
                    if portValid == true {
                        let weightParser: ISSCPWeightParser = StarIoExt.createScaleWeightParser(StarIoExtScaleModel.APS20)
                        
                        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
                        
                        GlobalQueueManager.shared.serialQueue.async {
//                          _ = Communication     .parseDoNotCheckCondition(weightParser, port: self.port, completionHandler: { (result: Bool, title: String, message: String) in
                            _ = ScaleCommunication.parseDoNotCheckCondition(weightParser, port: self.port, completionHandler: { (result: Bool, title: String, message: String) in
                                if result == true {
                                    DispatchQueue.main.async(execute: {
                                        self.scaleStatus = ScaleStatus.connect
                                        
                                        switch weightParser.status().rawValue {
                                        case StarIoExtDisplayedWeightStatus.zero.rawValue :
                                            self.commentLabel.text = weightParser.weight()
                                            
//                                          self.commentLabel.textColor = UIColor.green
                                            self.commentLabel.textColor = UIColor(red: 0.0, green: 0.7, blue: 0.0, alpha: 1.0)
                                            
                                            self.endAnimationCommantLabel()
                                        case StarIoExtDisplayedWeightStatus.notInMotion.rawValue :
                                            self.commentLabel.text = weightParser.weight()
                                            
                                            self.commentLabel.textColor = UIColor.blue
                                            
                                            self.endAnimationCommantLabel()
//                                      case StarIoExtDisplayedWeightStatus.motion.rawValue :
                                        default                                             :
                                            self.commentLabel.text = weightParser.weight()
                                            
                                            self.commentLabel.textColor = UIColor.red
                                            
                                            self.endAnimationCommantLabel()
                                        }
                                    })
                                }
                                else {
                                    let parser: ISCPConnectParser = StarIoExt.createScaleConnectParser(StarIoExtScaleModel.APS20)
                                    
                                    _ = Communication.parseDoNotCheckCondition(parser, port: self.port, completionHandler: { (result: Bool, title: String, message: String) in
                                        DispatchQueue.main.async(execute: {
                                            if result == true {
                                                if parser.connect() == true {     // Because the scale doesn't sometimes react.
                                                }
                                                else {
                                                    if self.scaleStatus != ScaleStatus.disconnect {
                                                        self.scaleStatus = ScaleStatus.disconnect
                                                        
                                                        self.commentLabel.text = "Scale Disconnect."
                                                        
                                                        self.commentLabel.textColor = UIColor.red
                                                        
                                                        self.beginAnimationCommantLabel()
                                                    }
                                                }
                                            }
                                            else {
                                                if self.scaleStatus != ScaleStatus.impossible {
                                                    self.scaleStatus = ScaleStatus.impossible
                                                    
//                                                  self.commentLabel.text = "Scale Impossible."
                                                    self.commentLabel.text = "Printer Impossible."
                                                    
                                                    self.commentLabel.textColor = UIColor.red
                                                    
                                                    self.beginAnimationCommantLabel()
                                                }
                                            }
                                        })
                                    })
                                }
                            })
                            
                            semaphore.signal()
                        }
                        
                        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            if self.scaleStatus != ScaleStatus.impossible {
                                self.scaleStatus = ScaleStatus.impossible
                                
//                              self.commentLabel.text = "Scale Impossible."
                                self.commentLabel.text = "Printer Impossible."
                                
                                self.commentLabel.textColor = UIColor.red
                                
                                self.beginAnimationCommantLabel()
                            }
                        })
                    }
                    
                    self.lock.unlock()
                }
                
                let timeout: DispatchTime
                
                if portValid == true {
                    timeout = DispatchTime.now() + Double(Int64(UInt64(0.2) * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)     // 200mS!!!
                }
                else {
                    timeout = DispatchTime.now() + Double(Int64(UInt64(1.0) * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)     // 1000mS!!!
                }
                
                if self.terminateTaskSemaphore.wait(timeout: timeout) == DispatchTimeoutResult.success {
                    self.terminateTaskSemaphore.signal()
                    
                    terminate = true
                }
            }
        }
    }
    
    func beginAnimationCommantLabel() {
        UIView.beginAnimations(nil, context: nil)
        
        self.commentLabel.alpha = 0.0
        
        UIView.setAnimationDelay             (0.0)                             // 0mS!!!
        UIView.setAnimationDuration          (0.6)                             // 600mS!!!
        UIView.setAnimationRepeatCount       (Float(UINT32_MAX))
        UIView.setAnimationRepeatAutoreverses(true)
        UIView.setAnimationCurve             (UIViewAnimationCurve.easeIn)
        
        self.commentLabel.alpha = 1.0
        
        UIView.commitAnimations()
    }
    
    func endAnimationCommantLabel() {
        self.commentLabel.layer.removeAllAnimations()
    }
}
