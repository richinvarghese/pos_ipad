//
//  Utf8MultiLanguageReceiptsImpl.swift
//  Swift SDK
//
//  Created by *** on 2018/**/**.
//  Copyright © 2018年 Star Micronics. All rights reserved.
//

import Foundation

class Utf8MultiLanguageReceiptsImpl: ILocalizeReceipts {
    override init() {
        super.init()
        
        languageCode = "CJK"
        
        characterCode = StarIoExtCharacterCode.standard
    }
    
    override func append2inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool, printData: String) {
        // not implemented
    }
    
    override func append3inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        let encoding: String.Encoding = String.Encoding.utf8
        
        // This function is supported by TSP650II(JP2/TW models only) with F/W version 4.0 or later.
        // Send the following command before each word and switch Kanji font.
        let preferDefault            = Data(bytes: [0x1b, 0x1d, 0x29, 0x55, 0x05, 0x00, 65, 0x00, 0x00, 0x00, 0x00])
        let preferJapanese           = Data(bytes: [0x1b, 0x1d, 0x29, 0x55, 0x05, 0x00, 65, 0x01, 0x00, 0x00, 0x00])    // <ESC><GS> ')' 'U' pL pH fn n1 n2 n3 n4
        let preferTraditionalChinese = Data(bytes: [0x1b, 0x1d, 0x29, 0x55, 0x05, 0x00, 65, 0x03, 0x00, 0x00, 0x00])    // <ESC><GS> ')' 'U' pL pH fn n1 n2 n3 n4
        let preferSimplifiedChinese  = Data(bytes: [0x1b, 0x1d, 0x29, 0x55, 0x05, 0x00, 65, 0x02, 0x00, 0x00, 0x00])    // <ESC><GS> ')' 'U' pL pH fn n1 n2 n3 n4
        let preferHangul             = Data(bytes: [0x1b, 0x1d, 0x29, 0x55, 0x05, 0x00, 65, 0x04, 0x00, 0x00, 0x00])    // <ESC><GS> ')' 'U' pL pH fn n1 n2 n3 n4

        builder.append(SCBCodePageType.UTF8)
        
        builder.appendCharacterSpace(0)
        
        builder.appendAlignment(SCBAlignmentPosition.center)
        
        builder.append("2017 / 5 / 15 AM 10:00\n".data(using: encoding))
        
        builder.append(preferJapanese)
        builder.append("受付票　".data(using: encoding))
        
        builder.append(preferTraditionalChinese)
        builder.append("驗收滑　".data(using: encoding))
        
        builder.append(preferSimplifiedChinese)
        builder.append("验收滑　 ".data(using: encoding))
        
        builder.append(preferHangul)
        builder.append("접수 표\n\n".data(using: encoding))
        
        builder.append(preferDefault)
        builder.appendData(withMultiple:"1\n".data(using: encoding), width:6, height:6)
        builder.append("------------------------------------------\n".data(using: encoding))

        builder.append(preferJapanese)
        builder.append("ご本人がお持ちください。\n".data(using: encoding))
        builder.append("※紛失しないようにご注意ください。\n".data(using: encoding))
    }
    
    override func append4inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        // not implemented
    }
    
    override func create2inchRasterReceiptImage() -> UIImage {
        // not implemented
        return UIImage()
    }
    
    override func create3inchRasterReceiptImage() -> UIImage {
        // not implemented
        return UIImage()
    }
    
    override func create4inchRasterReceiptImage() -> UIImage {
        // not implemented
        return UIImage()
    }
    
    override func createCouponImage() -> UIImage {
        // not implemented
        return UIImage()
    }
    
    override func createEscPos3inchRasterReceiptImage() -> UIImage {
        // not implemented
        return UIImage()
    }
    
    override func appendEscPos3inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        // not implemented
    }
    
    override func appendDotImpact3inchTextReceiptData(_ builder: ISCBBuilder, utf8: Bool) {
        // not implemented
    }
    
    override func appendTextLabelData(_ builder: ISCBBuilder, utf8: Bool) {
        // not implemented
    }
    
    override func createPasteTextLabelString() -> String {
        // not implemented
        return ""
    }
    
    override func appendPasteTextLabelData(_ builder: ISCBBuilder, pasteText: String, utf8: Bool) {
        // not implemented
    }
}
