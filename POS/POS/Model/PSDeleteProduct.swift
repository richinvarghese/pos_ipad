//
//  PSDeleteProduct.swift
//  POS
//
//  Created by Richin Varghese on 30/09/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSDeleteProduct: NSObject {
    
    var productName : String?
    var categoryName : String?
    var productCode : String?
    var categoryCode : String?
    var id : Int?
    var qty : Int?
    var price : String?
    var subTotal : Double?
    var discount : Double? = 0
    
    
    
}

