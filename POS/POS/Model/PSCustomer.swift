//
//  PSCustomer.swift
//  POS
//
//  Created by Richin Varghese on 07/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import Foundation

class PSCustomer: NSObject {
    
    var fullname:String?
    var email:String?
    var mobile:String?
    var id:String?
}
