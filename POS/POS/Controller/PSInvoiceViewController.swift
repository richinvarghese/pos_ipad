//
//  PSInvoiceViewController.swift
//  POS
//
//  Created by Richin Varghese on 16/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD

class PSInvoiceViewController:  CommonViewController, UITableViewDelegate, UITableViewDataSource, StarIoExtManagerDelegate, UIAlertViewDelegate {
  
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var cnameLabel: UILabel!
    @IBOutlet weak var orderTableView: UITableView!
    
    @IBOutlet weak var returnChargeLabel: UILabel!
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var paidAmount: UILabel!
    var addToCart = [PSProduct]()
     var order = PSOrder()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        grandTotal.text = "\(order.grandTotal!)"
        paidAmount.text = order.paidAmount
        returnChargeLabel.text = order.returnCharge
        
        if let gd = order.tempGrandTotal{
            
            let tempValue = order.grandTotal! - gd
         //   grandTotal.text = "\(String(tempValue))"
           // paidAmount.text = "\(String(tempValue))"
            if(tempValue<0){
                returnChargeLabel.text = order.paidAmount
            }
            
        }
        
        
        let string = UserDefaults.standard.object(forKey: "logo")
        let cname = UserDefaults.standard.object(forKey: "cname")

        let url = URL(string: string as! String)
        
        logoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
      cnameLabel.text = "\(cname!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addToCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderTableView.dequeueReusableCell(withIdentifier: "PsInvoiceTableViewCell") as! PsInvoiceTableViewCell
        if let q = addToCart[indexPath.row].qty {
            
           cell.qtyLabel!.text = "\(q)"
        }
        cell.productLabel!.text = addToCart[indexPath.row].productName
        cell.slnoLabel!.text = "\(indexPath.row+1)"
        if let p = addToCart[indexPath.row].price {
            
            cell.perPriceLabel!.text = "\(p)"
        }
        if let s = addToCart[indexPath.row].subTotal {
            
            cell.subTotalLabel!.text = "\(s)"
        }
        // cell.qtyLabel!.text = "\(addToCart[indexPath.row].qty)"
        // print(addToCart[indexPath.row].productName)
      /*  cell.itemNameLabel!.text = addToCart[indexPath.row].productName
        cell.categoryLabel!.text = addToCart[indexPath.row].categoryName
        cell.priceLabel!.text = addToCart[indexPath.row].price*/
        return cell
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
        showDetailViewController(vc, sender: self)
    }
    
    @IBAction func printButtonPressed(_ sender: UIButton) {
        
        
        let portName: String = AppDelegate.getPortName()
        let portSettings: String = AppDelegate.getPortSettings()
        
        for _ in 1..<2{
     
        var printData:String = ""
        var addString : String = ""
        // printData = "SKU         Description    Total\n"
            addString =  "Sales ID : "+order.salesID+"\n\n"
            printData = printData + addString
            
        printData = printData + "SKU         Description    Discount    Total\n"
        for cart in addToCart {
            //print(cart.productCode!+"\n")
            addString = ""
            addString = cart.productCode!
            addString = addString.padding(toLength: 12, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart.productName!
            addString = addString.padding(toLength: 15, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = "\(cart.discount!)"
            addString = addString.padding(toLength: 12, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart.price!
            addString = addString.padding(toLength: 10, withPad: " ", startingAt: 0)
            printData = printData + addString
           // printData = printData.padding(toLength: 12, withPad: " ", startingAt: 0)
            //printData = printData+cart.productName!
            //printData = printData
            printData = printData + "\n"
            
            //+"   "+cart.productName!+"   "+cart.price!+"\n"
                
                
                //+ "   "+ cart.productName!
           // printData += "   " + cart.price!+"\n"
            
            
        }
        printData = printData + "\n"
        
        addString = "Subtotal                    "
        printData = printData + addString
        printData = printData+"\(order.subTotal!)"+"\n"
        addString = "Discount                    "
        printData = printData + addString
        printData = printData+"\(order.discountAmt!)"+"\n"
        addString = "Tax                         "
        printData = printData+addString
        printData = printData+"\(order.tax!)"+"\n"
        printData = printData+"--------------------------------\n"
        addString = "Grand Total                 "
        printData = printData+addString
        printData = printData+"\(order.grandTotal!)"
        printData = printData+"\n"
      //  print(printData)
        
        
       /* builder.append((
            "SKU         Description    Total\n" +
                "300678566   PLAIN T-SHIRT  10.99\n" +
                "300692003   BLACK DENIM    29.99\n" +
                "300651148   BLUE DENIM     29.99\n" +
                "300642980   STRIPED DRESS  49.99\n" +
                "300638471   BLACK BOOTS    35.99\n" +
                "\n" +
                "Subtotal                  156.95\n" +
                "Tax                         0.00\n" +
            "--------------------------------\n").data(using: encoding))*/
        
        
         let commands: Data
        let emulation: StarIoExtEmulation = AppDelegate.getEmulation()
        
        let width: Int = AppDelegate.getSelectedPaperSize().rawValue
        
        let localizeReceipts: ILocalizeReceipts = LocalizeReceipts.createLocalizeReceipts(AppDelegate.getSelectedLanguage(), paperSizeIndex: AppDelegate.getSelectedPaperSize())
        
          if AppDelegate.getSelectedLanguage() != LanguageIndex.cjkUnifiedIdeograph {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: false, printData: printData)

        }
          else {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: true,  printData: printData)
        }
        
        self.blind = true
        
        
        
        
        GlobalQueueManager.shared.serialQueue.async {
            _ = Communication.sendCommands(commands,
                                           portName: portName,
                                           portSettings: portSettings,
                                           timeout: 10000,  // 10000mS!!!
                completionHandler: { (result: Bool, title: String, message: String) in
                    DispatchQueue.main.async {
                        let alertView: UIAlertView = UIAlertView(title: title,
                                                                 message: message,
                                                                 delegate: nil,
                                                                 cancelButtonTitle: "OK")
                        
                      //  alertView.show()
                        
                        self.blind = false
                    }
            })
        }


        }
        let commands1: Data
        commands1 = CashDrawerFunctions.createData(AppDelegate.getEmulation(), channel: SCBPeripheralChannel.no1)
        
        // let portName:     String = AppDelegate.getPortName()
        //let portSettings: String = AppDelegate.getPortSettings()
        let timeout:      UInt32 = 10000                             // 10000mS!!!
        self.blind = true
        GlobalQueueManager.shared.serialQueue.async {
            Communication.sendCommands(commands1,
                                       portName: portName,
                                       portSettings: portSettings,
                                       timeout: timeout,
                                       completionHandler: { (result: Bool, title: String, message: String) in
                                        DispatchQueue.main.async {
                                            let alertView: UIAlertView = UIAlertView(title: title,
                                                                                     message: message,
                                                                                     delegate: nil,
                                                                                     cancelButtonTitle: "OK")
                                            
                                            alertView.show()
                                            
                                            self.blind = false
                                        }
            })
            
            
        }
        
       
    }
    
    
    
   
   
    
    
}
