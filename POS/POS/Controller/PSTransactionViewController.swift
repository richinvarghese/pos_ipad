//
//  PSTransactionViewController.swift
//  POS
//
//  Created by Richin Varghese on 04/09/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON


class PSTransactionViewController: CommonViewController, UITableViewDelegate, UITableViewDataSource, StarIoExtManagerDelegate, UIAlertViewDelegate, UISearchBarDelegate {
  
    @IBOutlet weak var salesSearchBar: UISearchBar!
    var reportArray = [NSDictionary]()
    var tempArray = [NSDictionary]()
    var summery = NSDictionary()
     var order = NSDictionary()
    var orderDetail = [NSDictionary]()
    var order_id: String?
    @IBOutlet weak var transactionTableView: UITableView!
    
    @IBOutlet weak var taxTotalLabel: UILabel!
    
    @IBOutlet weak var grandTotalLabel: UILabel!
    
    @IBOutlet weak var subTotalLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewReport()
        salesSearchBar.delegate = self
        self.splitViewController?.preferredDisplayMode = .automatic
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return reportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = transactionTableView.dequeueReusableCell(withIdentifier: "PSTransactionTableViewCell") as! PSTransactionTableViewCell
        
        cell.dateLabel.text = reportArray[indexPath.row]["order_dtm"] as! String
        
        cell.salesIDLabel.text = reportArray[indexPath.row]["order_id"] as! String
        
       // cell.paymentLabel.text = reportArray[indexPath.row]["payment_method_name"] as! String
         cell.paymentLabel.text = reportArray[indexPath.row]["subTotal"] as! String
        cell.subtotalLabel.text = reportArray[indexPath.row]["tax"] as! String
        
        cell.discountLabel.text = reportArray[indexPath.row]["grandTotal"] as! String

        cell.editObject = {
          //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSAddProductViewController") as! PSAddProductViewController
            if let id   = self.reportArray[indexPath.row]["order_id"] as? String{
                self.order_id = id
                self.getInvoice()
                //vc.productId = id
                            //    self.showDetailViewController(vc, sender: self)
            }
            
}
        cell.voidObject = {
             if let id   = self.reportArray[indexPath.row]["order_id"] as? String{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
                vc.orderID = id
                //vc.productId = id
                    self.showDetailViewController(vc, sender: self)
            }
            
        }
        /*cell.editObject = {

            if let id   = self.reportArray[indexPath.row]["id"] as? String{
               
               print(id)
            }
            
        }*/
      //  cell.grandTotoal.text =
        
        return cell
        
    }
    
    func viewReport(){
        // print(paymentID!)
        
        self.reportArray.removeAll();
        
        self.transactionTableView.reloadData()
        let url = PSCommon.Global.baseURL+"report/sales"
        ProgressHUD.show()
        var body: Dictionary<String,AnyObject>?
        
        body = ["outlet":"1", "paid": "0"] as [String:AnyObject]
        
     //   body = ["outlet":"1", "paid": "0", "start_date":startTextField.text!, "end_date":endTextField.text!] as [String:AnyObject]
        
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                print(json)
                
                if let resultDict = response.result.value as? NSDictionary {
                    
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"]  as? NSDictionary {
                            if let arrayDict  = dict["report"] as? [NSDictionary]{
                              //  print(arrayDict)
                                self.reportArray = arrayDict
                                self.tempArray = arrayDict
                                self.transactionTableView.reloadData()
                            }
                            
                            if let sum = dict["summery"] as? NSDictionary{
                                // print(arrayDict)
                                // self.summery = arrayDict
                                
                                if let t = sum["tax_amt"] {
                                    
                                    self.taxTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                if let t = sum["sub_amt"] {
                                    
                                    self.subTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                if let t = sum["grand_amt"] {
                                    
                                    self.grandTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                
                            }
                            
                        }
                    }
                }else{
                    
                    
                    self.reportArray.removeAll();
                    
                    self.transactionTableView.reloadData()
                    
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    
    func getInvoice(){
        let url = PSCommon.Global.baseURL+"report/invoice"
        ProgressHUD.show()
        var body: Dictionary<String,AnyObject>?
        
        body = ["order_id":order_id] as [String:AnyObject]
        
        //   body = ["outlet":"1", "paid": "0", "start_date":startTextField.text!, "end_date":endTextField.text!] as [String:AnyObject]
        
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                 // print(json)
                
                if let resultDict = response.result.value as? NSDictionary {
                    
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"]  as? NSDictionary {
                            if let orderDict  = dict["order_details"] as? [NSDictionary]{
                                
                                self.orderDetail = orderDict
                                
                               
                            }
                            
                            if let sum = dict["order"] as? NSDictionary{
                                // print(arrayDict)
                                // self.summery = arrayDict
                                   self.order = sum
                                
                                
                            }
                            
                            self.rePrint()
                            
                        }
                    }
                }else{
                    
                 
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
    }
    
    func rePrint(){
        var printData:String = ""
        var addString : String = ""
        // printData = "SKU         Description    Total\n"
        addString =  "Sales ID : \(order["sales_id"]!)\n\n"
        printData = printData + addString
        
        printData = printData + "SKU         Description    Discount    Total\n"
        for cart in orderDetail {
            //print(cart.productCode!+"\n")
            addString = ""
            addString = "\(cart["pcode"]!)"
            addString = addString.padding(toLength: 12, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart["name"] as! String
            addString = addString.padding(toLength: 15, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart["discount"] as! String
            addString = addString.padding(toLength: 12, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart["price"] as! String
            addString = addString.padding(toLength: 10, withPad: " ", startingAt: 0)
            printData = printData + addString
            
            printData = printData + "\n"
      
        }
        printData = printData + "\n"
        
        addString = "Subtotal                    "
        printData = printData + addString
        printData = printData+"\(order["subTotal"]!)"+"\n"
        addString = "Discount                    "
        printData = printData + addString
        printData = printData+"\(order["dis_amt"]!)"+"\n"
       /* addString = "Tax                         "
        printData = printData+addString
        printData = printData+"\(order["tax"]!)"+"\n"*/
        printData = printData+"--------------------------------\n"
        addString = "Grand Total                 "
        printData = printData+addString
        printData = printData+"\(order["grandTotal"]!)"
        printData = printData+"\n"
       
       // print(printData)
        
        let commands: Data
        let emulation: StarIoExtEmulation = AppDelegate.getEmulation()
        
        let width: Int = AppDelegate.getSelectedPaperSize().rawValue
        
        let localizeReceipts: ILocalizeReceipts = LocalizeReceipts.createLocalizeReceipts(AppDelegate.getSelectedLanguage(), paperSizeIndex: AppDelegate.getSelectedPaperSize())
        
        if AppDelegate.getSelectedLanguage() != LanguageIndex.cjkUnifiedIdeograph {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: false, printData: printData)
            
        }
        else {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: true,  printData: printData)
        }
        
        self.blind = true
        
        let portName: String = AppDelegate.getPortName()
        let portSettings: String = AppDelegate.getPortSettings()
        
        
        
        GlobalQueueManager.shared.serialQueue.async {
            _ = Communication.sendCommands(commands,
                                           portName: portName,
                                           portSettings: portSettings,
                                           timeout: 10000,  // 10000mS!!!
                completionHandler: { (result: Bool, title: String, message: String) in
                    DispatchQueue.main.async {
                        let alertView: UIAlertView = UIAlertView(title: title,
                                                                 message: message,
                                                                 delegate: nil,
                                                                 cancelButtonTitle: "OK")
                        
                      //    alertView.show()
                        
                        self.blind = false
                    }
            })
        }
        
        

            
        

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        reportArray = (searchText.isEmpty ? tempArray : reportArray.filter({(dataString: NSDictionary) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return (dataString["order_id"] as! String).range(of: searchText, options: .caseInsensitive) != nil
        }))
        
        self.transactionTableView.reloadData()
    }

}
