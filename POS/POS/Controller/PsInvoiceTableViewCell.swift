//
//  PsInvoiceTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 16/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PsInvoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var qtyLabel: UILabel!
    
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var perPriceLabel: UILabel!
    @IBOutlet weak var slnoLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
