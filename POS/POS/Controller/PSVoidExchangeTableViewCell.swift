//
//  PSVoidExchangeTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 04/10/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSVoidExchangeTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var saleIdLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    var showObject : (() -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func showButtonPressed(_ sender: UIButton) {
        if let btnAction = self.showObject {
            btnAction()
        }
    }
    
}
