//
//  PSGiftCardViewController.swift
//  POS
//
//  Created by Richin Varghese on 13/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON


class PSGiftCardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var giftCardTableView: UITableView!
    
    @IBOutlet weak var expiryView: UIView!
    
    var giftCardArray = [NSDictionary]()
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewGiftCard()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return giftCardArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = giftCardTableView.dequeueReusableCell(withIdentifier: "PSGiftCardTableViewCell") as! PSGiftCardTableViewCell
       
        if let cardNumber = giftCardArray[indexPath.row]["card_number"] as? String{
            cell.giftCardNumberLabel!.text = cardNumber
        }
        else{
            cell.giftCardNumberLabel!.text = "Nil"
        }
        if let value = giftCardArray[indexPath.row]["value"] as? String{
            cell.valueLabel!.text = value
           // print(value)
        }
        if let expiry   = giftCardArray[indexPath.row]["expiry_date"] as? String{
            cell.expiryDateLabel!.text = expiry
        }
        
        if let status   = giftCardArray[indexPath.row]["status"] as? String{
            cell.statusLabel!.text = status
        }
        
        return cell
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
      //  navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCusomerButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSAddGiftCardViewController") as! PSAddGiftCardViewController
        showDetailViewController(vc, sender: self);
    }
    
    func viewGiftCard(){
     
        let url = PSCommon.Global.baseURL+"gift_card/view"
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                 print("JSON: \(json)")
                 // print(response.result.value)
                
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"]as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.giftCardArray = dict
                           // print(self.giftCardArray)
                            self.giftCardTableView.reloadData()
                        }
                    }
                }else{
                    let message = json["message"].string
                    //print(message)
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    

}
