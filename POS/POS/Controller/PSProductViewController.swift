//
//  PSProductViewController.swift
//  POS
//
//  Created by Richin Varghese on 07/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON
import SDWebImage

class PSProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var productTableView: UITableView!
    var productArray = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewProduct()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addProductButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSAddProductViewController") as! PSAddProductViewController
        showDetailViewController(vc, sender: self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = productTableView.dequeueReusableCell(withIdentifier: "PSProductTableViewCell") as! PSProductTableViewCell
        
        if let code = productArray[indexPath.row]["code"] as? String{
            cell.codeLabel!.text = code
            if let image = productArray[indexPath.row]["thumbnail"] as? String {
                let imageURL = URL(string: PSCommon.Global.thumbnail+code+"/"+image)
                //print(imageURL);
                cell.productImage?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
                
                
                
            }
        }
        else{
            cell.codeLabel!.text = "Nil"
        }
        if let name = productArray[indexPath.row]["name"] as? String{
            cell.nameLabel!.text = name
        }
        if let category   = productArray[indexPath.row]["catname"] as? String{
            cell.categoryLabel!.text = category
        }
        if let price   = productArray[indexPath.row]["purchase_price"] as? String{
            cell.priceLabel!.text = price
        }
        if let status   = productArray[indexPath.row]["purchase_price"] as? String{
            if status == "0" {
                cell.statusLabel!.text = "Inactive"
            }
            else {
                cell.statusLabel!.text = "Active"
            }
        }
        cell.editObject = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSAddProductViewController") as! PSAddProductViewController
            if let id   = self.productArray[indexPath.row]["id"] as? String{
                vc.productId = id
                self.showDetailViewController(vc, sender: self)
            }
            
        }
        
        return cell;
    }
    
    func viewProduct(){
        
        let url = PSCommon.Global.baseURL+"products/view"
        ProgressHUD.show()
        //print(url)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                //your success code
                // print(response)
                let json = JSON(value)
              //  print("JSON: \(json)")
                //  print(response.result.value)
                
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"]as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.productArray = dict
                            //print(self.productArray)
                            self.productTableView.reloadData()
                            
                            
                            
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }

}
