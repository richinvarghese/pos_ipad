//
//  PSShowOrderViewController.swift
//  POS
//
//  Created by Richin Varghese on 09/10/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSShowOrderViewController: CommonViewController, UITableViewDelegate, UITableViewDataSource, StarIoExtManagerDelegate { 
    
    var result = PSAPIClient()
    var voidOrderArray : NSDictionary = [:]
     var voidOrderItems = [NSDictionary]()
    var list = [NSDictionary]()
    var sectionCount : Int?
    
    var gt = 0.0
    @IBOutlet weak var paymentMethodLabel: UILabel!
    var voidParentArray : NSDictionary = [:]
    
    var order_id : String?

   
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var companyNameLabel: UILabel!
    
    @IBOutlet weak var subtotalLabel: UILabel!
    
    @IBOutlet weak var orderTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let string = UserDefaults.standard.object(forKey: "logo")
        let cname = UserDefaults.standard.object(forKey: "cname")
        companyNameLabel.text = "\(cname!)"
        let url = URL(string: string as! String)
        logoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        voidOrder()
        //fillSummery()
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return voidOrderItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderTableView.dequeueReusableCell(withIdentifier: "PSShowOrderTableViewCell") as! PSShowOrderTableViewCell
        cell.slnoLabel.text = "\(indexPath.row+1)"
        cell.productLabel.text = voidOrderItems[indexPath.row]["product_name"] as! String
        cell.qtyLabel.text = voidOrderItems[indexPath.row]["qty"] as! String
         cell.perItemLabel.text = voidOrderItems[indexPath.row]["price"] as! String
        let q  = (cell.qtyLabel.text as! NSString).doubleValue
        let p  = (cell.perItemLabel.text as! NSString).doubleValue
      cell.totalLabel.text = "\(q*p)"
        return cell
    }
    
    
    func voidOrder()
    {
        let parms = ["outlet":"1", "order_id": order_id] as [String:AnyObject]
        PSAPIClient.VoidOrder(params: parms) { (response, message, success) in
            if(success){
                self.result = response as! PSAPIClient
                
                if let s = self.result.voidDictionary["message"] {
                    
                    if s as! String == "success"{
                        //  print(s)
                        if let res =  self.result.voidDictionary["response"] {
                            let r = res as! [String:Any]
                            let ord  = r["order_child"] as? NSDictionary
                            let ordInfo = ord as! [String:Any]
                            if let orderDetails = ordInfo["order"] as? NSDictionary{
                                self.voidOrderArray = orderDetails
                                
                            }
                            if let orderDetails = ordInfo["items"] as? [NSDictionary]{
                                self.voidOrderItems = orderDetails
                                //print(self.voidOrderItems)
                                
                            }
                            let ordp  = r["order_parent"] as? NSDictionary
                            let ordInfop = ordp as! [String:Any]
                            if let orderDetails = ordInfop["order"] as? NSDictionary{
                                self.voidParentArray = orderDetails
                                // print(self.voidParentArray)
                                
                            }
                            self.orderTableView.reloadData()
                            self.fillSummery()
                            
                        }
                        else{
                            print("er")
                        }
                        
                    }
                        
                    else {
                        print("error")
                    }
                }
                
                
            }
            
        }
    }
    
    func fillSummery(){
        var childGT = 0.0
        var parentGT = 0.0
        if let temp = voidOrderArray["grandtotal"] {
            childGT = (temp as! NSString).doubleValue
            print("child" + "\(temp)")
        }
        if let temp = voidParentArray["grandtotal"] {
            parentGT = (temp as! NSString).doubleValue
            print("parent" + "\(temp)")
        }
        gt = (childGT + parentGT)
        
        if let subTotal = voidOrderArray["subtotal"]  {
            subtotalLabel.text = "\(subTotal)"
        }
        if  let paymentMethod = voidOrderArray["payment_method_name"] {
            paymentMethodLabel.text = "\(paymentMethod)"
        }
        
        grandTotalLabel.text = "\(gt)"
        
        
    }
    
    
    @IBAction func printButtonPressed(_ sender: UIButton) {
       rePrint()
    }
    
    func rePrint(){
        var printData:String = ""
        var addString : String = ""
        // printData = "SKU         Description    Total\n"
        addString =  "Sales ID : \(order_id!)\n\n"
        printData = printData + addString
        
        printData = printData + "SKU             Description              Qty    Total\n"
        for cart in voidOrderItems {
            //print(cart.productCode!+"\n")
            addString = ""
            addString = "\(cart["product_code"]!)"
            addString = addString.padding(toLength: 16, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart["product_name"] as! String
            addString = addString.padding(toLength: 25, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart["qty"] as! String
            addString = addString.padding(toLength: 7, withPad: " ", startingAt: 0)
            printData = printData + addString
            addString = ""
            addString = cart["price"] as! String
            addString = addString.padding(toLength: 10, withPad: " ", startingAt: 0)
            printData = printData + addString
            
            printData = printData + "\n"
            
        }
        printData = printData + "\n"
        
        addString = "Subtotal                    "
        printData = printData + addString
        printData = printData+"\(voidOrderArray["subtotal"]!)"+"\n"
        addString = "Payment Method               "
        printData = printData + addString
        printData = printData+"\(voidOrderArray["payment_method_name"]!)"+"\n"
        /* addString = "Tax                         "
         printData = printData+addString
         printData = printData+"\(order["tax"]!)"+"\n"*/
        printData = printData+"--------------------------------\n"
        addString = "Grand Total                 "
        printData = printData+addString
        printData = printData+"\(gt)"
        printData = printData+"\n"
        
         print(printData)
        
        let commands: Data
        let emulation: StarIoExtEmulation = AppDelegate.getEmulation()
        
        let width: Int = AppDelegate.getSelectedPaperSize().rawValue
        
        let localizeReceipts: ILocalizeReceipts = LocalizeReceipts.createLocalizeReceipts(AppDelegate.getSelectedLanguage(), paperSizeIndex: AppDelegate.getSelectedPaperSize())
        
        if AppDelegate.getSelectedLanguage() != LanguageIndex.cjkUnifiedIdeograph {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: false, printData: printData)
            
        }
        else {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: true,  printData: printData)
        }
        
        self.blind = true
        
        let portName: String = AppDelegate.getPortName()
        let portSettings: String = AppDelegate.getPortSettings()
        
        
        
        GlobalQueueManager.shared.serialQueue.async {
            _ = Communication.sendCommands(commands,
                                           portName: portName,
                                           portSettings: portSettings,
                                           timeout: 10000,  // 10000mS!!!
                completionHandler: { (result: Bool, title: String, message: String) in
                    DispatchQueue.main.async {
                        let alertView: UIAlertView = UIAlertView(title: title,
                                                                 message: message,
                                                                 delegate: nil,
                                                                 cancelButtonTitle: "OK")
                        
                           alertView.show()
                        
                        self.blind = false
                    }
            })
        }
        
        
        
        
        
        
    }
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount!
    }*/
    

}
