//
//  PSSalesHistoryViewController.swift
//  POS
//
//  Created by Richin Varghese on 20/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSSalesHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var salesHistoryTable: UITableView!
    @IBOutlet weak var taxTotalLabel: UILabel!
    
    @IBOutlet weak var grandTotalLabel: UILabel!
    var historyArray = [NSDictionary]()
    var summery = NSDictionary()
    var customerId:String?

    @IBOutlet weak var subTotalLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(customerId)
        viewSales()
       // salesHistoryTable.delegate = self
        //salesHistoryTable.dataSource = self

        // Do any additional setup after loading the view.
       
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = salesHistoryTable.dequeueReusableCell(withIdentifier: "PSSalesHistoryTableViewCell") as! PSSalesHistoryTableViewCell
      //  cell.qtyLabel.text = "34"
        
        if let salesID = historyArray[indexPath.row]["sales_id"] as? String{
            cell.saleIdLabel.text = salesID
        }
        else{
            cell.saleIdLabel!.text = ""
        }
        if let type = historyArray[indexPath.row]["type"] as? String{
            cell.typeLabel!.text = type
        }
        if let dtm   = historyArray[indexPath.row]["dtm"] as? String{
            cell.dateLabel!.text = dtm
        }
        if let payment   = historyArray[indexPath.row]["payment_method_name"] as? String{
            cell.productLabel!.text = payment
        }
        if let vtStatus   = historyArray[indexPath.row]["vt_status"] as? String{
            cell.qtyLabel!.text = vtStatus
        }
        if let totalItems   = historyArray[indexPath.row]["total_items"] as? String{
            cell.totalQty!.text = totalItems
        }
        if let subtotal   = historyArray[indexPath.row]["subTotal"] as? String{
            cell.subTotalLabel!.text = subtotal
        }
        if let tax   = historyArray[indexPath.row]["tax"] as? String{
            cell.taxLabel!.text = tax
        }
        if let grandTotal   = historyArray[indexPath.row]["grandTotal"] as? String{
            cell.grandTotalLabel!.text = grandTotal
        }
        return cell
    }
    
    
    func viewSales(){
        
        let url = PSCommon.Global.baseURL+"customers/history"
        ProgressHUD.show()
        
        var body: Dictionary<String,AnyObject>?
        
        body = ["cust_id":customerId] as [String:AnyObject]
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("JSON: \(json)")
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"]  as? NSDictionary {
                            if let arrayDict  = dict["sales"] as? [NSDictionary]{
                                self.historyArray = arrayDict
                                self.salesHistoryTable.reloadData()
                                 //print(self.historyArray)
                                
                            }
                            if let sum = dict["summery"] as? NSDictionary {
                                self.summery = sum
                               // print(sum)
                                if let t = sum["tax_total"] {
                                    
                                    self.taxTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                if let t = sum["sub_total"] {
                                    
                                    self.subTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                if let t = sum["grand_total"] {
                                    
                                    self.grandTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                
                            }
                            
                        
                        }
                    }
                    else{
                        let message = json["message"].string
                        // print(message)
                        let alertController = UIAlertController(title: "Error", message:
                            message, preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                else{
                    
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    
    
    
    
}
