//
//  PSCategoryCollectionViewCell.swift
//  POS
//
//  Created by Richin Varghese on 14/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var poductNameLabel: UILabel!
}
