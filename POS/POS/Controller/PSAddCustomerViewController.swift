//
//  PSAddCustomerViewController.swift
//  POS
//
//  Created by Richin Varghese on 04/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSAddCustomerViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    var customerId:String?
     var customer = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if customerId != nil {
            getCustomer()
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPresses(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func addButtonPressesd(_ sender: UIButton) {
     
        addCustomer(name: nameTextField.text!, email: emailTextField.text!, mobile: mobileTextField.text!)
        
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func addCustomer(name: String, email: String, mobile: String){
         var body: Dictionary<String,AnyObject>?
        var url: String
        
        if let custID = customerId{
            url = PSCommon.Global.baseURL+"customers/update"
            body = ["user_id":"1", "cust_id":custID, "fullname":nameTextField.text!, "email":emailTextField.text!, "mobile":mobileTextField.text!] as [String:AnyObject]
          

        }
        else{
            url = PSCommon.Global.baseURL+"customers/insert"
            body = ["created_user_id":"1", "fullname":nameTextField.text!, "email":emailTextField.text!, "mobile":mobileTextField.text!] as [String:AnyObject]
        }
       
        
        
         ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("JSON: \(json)")
                if json["status"] == "success" {
                    print(json["message"])
                    
                    let alertController = UIAlertController(title: "Success", message:
                        "Saved", preferredStyle: UIAlertControllerStyle.alert)
                   
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSCustomerViewController")
                        as! PSCustomerViewController
                    self.showDetailViewController(vc, sender: self)
                    
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    func getCustomer(){
        let url = PSCommon.Global.baseURL+"customers/edit"
        var body: Dictionary<String,AnyObject>?
        body = ["cust_id":customerId] as [String:AnyObject]
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("JSON: \(json)")
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        if let dict = resultDict["message"]  as? NSDictionary {
                          
                            if let custID = dict["cust_id"]  {
                               self.customerId = "\(custID)"
                            }
                            if let fn = dict["fullname"]  {
                                self.nameTextField.text = "\(fn)"
                            }
                            if let email = dict["email"]  {
                                self.emailTextField.text = "\(email)"
                            }
                            if let mobile = dict["mobile"]  {
                                self.mobileTextField.text = "\(mobile)"
                            }
                            
                        }
                    }
                    else{
                        let message = json["message"].string
                        // print(message)
                        let alertController = UIAlertController(title: "Error", message:
                            message, preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
                
                else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }

   
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSCustomerViewController")
            as! PSCustomerViewController
        showDetailViewController(vc, sender: self)
    }
}
