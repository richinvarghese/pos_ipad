//
//  PSGiftCardTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 13/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSGiftCardTableViewCell: UITableViewCell {

    @IBOutlet weak var giftCardNumberLabel: UILabel!
   
   
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var expiryDateLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
