//
//  PSPaymentUpdateViewController.swift
//  POS
//
//  Created by Richin Varghese on 23/08/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import Alamofire
import ProgressHUD
import SwiftyJSON

class PSPaymentUpdateViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    var paymentID: String?
    var paymentName: String?
    var paymentStatus: String?
    
    let status = ["Select","Active", "Inactive"]
    let statusPicker = UIPickerView()
    
   
    @IBOutlet weak var paymentSatausTextField: UITextField!
 
    @IBOutlet weak var paymentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusPicker.delegate = self
        paymentSatausTextField.inputView = statusPicker
        
        paymentTextField.text = paymentName
        if paymentStatus  == "1" {
        paymentSatausTextField.text = "Active"
        }
        else{
            paymentSatausTextField.text = "Inactive"
        }

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSSettingsTableViewController") as! PSSettingsTableViewController
        showDetailViewController(vc, sender: self)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return status.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return status[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        paymentSatausTextField.text =   status[row]
        if row == 1 {
            paymentStatus = "1"
        }
        else if row == 2 {
            paymentStatus = "0"
        }
    }
    
    func addPayment(){
        
        var body: Dictionary<String,AnyObject>?
        var url: String
        
        if let payID = paymentID {
            url = PSCommon.Global.baseURL+"settings/paymentupdate"
            body = ["id":paymentID, "name":paymentTextField.text,  "status":paymentStatus] as [String:AnyObject]
        }
        else{
            url = PSCommon.Global.baseURL+"settings/paymentupdate"
            body = ["id":paymentID, "name":paymentTextField.text,  "status":paymentStatus] as [String:AnyObject]        }
        
        
        
        
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                // print("JSON: \(json)")
                if json["status"] == "success" {
                    // print(json["message"])
                    if self.paymentID != nil {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSPaymentViewController") as! PSPaymentViewController
                        self.showDetailViewController(vc, sender: self)
                    }
                    let alertController = UIAlertController(title: "Success", message:
                        "Saved", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Success", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        addPayment()
    }
    
    
    
}
