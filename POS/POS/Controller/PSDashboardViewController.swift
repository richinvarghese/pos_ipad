//
//  PSDashboardViewController.swift
//  POS
//
//  Created by Richin Varghese on 27/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import Alamofire
import ProgressHUD
import SwiftyJSON
class PSDashboardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var cname: String = "";
    var caddress: String = "";
    @IBOutlet weak var dashboardCollectionView: UICollectionView!
    
    let menu = ["Point Of Sales", "Gift Card", "Today's Sale", "Products", "Customers", "Settings "]
    
    
    let menuImages: [UIImage] = [
        UIImage(named: "pos")!,
        UIImage(named: "sales")!,
        UIImage(named: "reports")!,
        UIImage(named: "outlets")!,
        UIImage(named: "users")!,
        UIImage(named: "settings")!
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.splitViewController?.preferredDisplayMode = .automatic

        dashboardCollectionView.dataSource = self
        dashboardCollectionView.delegate = self
        
        var layout  = self.dashboardCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 40, right: 10)
        layout.minimumInteritemSpacing = 10
        layout.itemSize = CGSize(width: (self.dashboardCollectionView.frame.size.width) / 3, height: self.dashboardCollectionView.frame.size.height / 4)
        
       
       let string = UserDefaults.standard.object(forKey: "logo")
        
        if let s = string
        {
        print(s)
        }
        else{
            print("logo not set")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        

        let vc = storyboard?.instantiateViewController(withIdentifier: "PSMenuViewController") as! PSMenuViewController
        navigationController?.show(vc, sender: self)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dashboardCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PSDashboardCollectionViewCell
        cell.dashboardMenuImageView.image = menuImages[indexPath.item]
        cell.menuNameLabel.text = menu[indexPath.item]
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
           //     cell?.layer.borderColor = UIColor(red: 247/255.0, green: 49/255.0, blue: 72/255.0, alpha: 1).cgColor
        let color1 = UIColor(hexString: "#F73148")
        cell?.layer.borderColor = color1.cgColor
        cell?.layer.borderWidth = 2
        
        switch indexPath.row {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
            showDetailViewController(vc, sender: self)
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSGiftCardViewController")
                as! PSGiftCardViewController
             showDetailViewController(vc, sender: self)
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSTodaySalesViewController")
                as! PSTodaySalesViewController
            showDetailViewController(vc, sender: self)
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSProductViewController")
                as! PSProductViewController
            showDetailViewController(vc, sender: self)
        case 4:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSCustomerViewController")
                as! PSCustomerViewController
            showDetailViewController(vc, sender: self)
        case 5:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSSettingsTableViewController") as! PSSettingsTableViewController
            showDetailViewController(vc, sender: self)
         //   let vc = storyboard?.instantiateViewController(withIdentifier: "PSPaymentViewController") as! PSPaymentViewController
           // showDetailViewController(vc, sender: self)
        default:
            print("Under Testing")
        }
        
       
       // navigationController?.show(vc, sender: self)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.darkGray.cgColor
        cell?.layer.borderWidth = 0.5
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func sideMenuPressed(_ sender: UIButton) {
         //preferredDisplayMode = .primaryOverlay
      //  self.splitViewController?.preferredDisplayMode = .automatic

    }
    
    
}


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue:      CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    
    
}


