//
//  PSReportViewController.swift
//  POS
//
//  Created by Richin Varghese on 21/08/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSReportViewController: CommonViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, StarIoExtManagerDelegate {
    
    
    @IBOutlet weak var reportTableView: UITableView!
    var payment = [NSDictionary]()
    var paymentPicker = UIPickerView()
    var paymentID: String? = "0"
    var datePickerStartDate: UIDatePicker?
    var datePickerEndDate: UIDatePicker?
    var totalAmount: String  = ""
     var totalNett: String  = ""
     var totalCash: String  = ""
    @IBOutlet weak var paymentTextFiled: UITextField!
    
    
    @IBOutlet weak var endTextField: UITextField!
    @IBOutlet weak var startTextField: UITextField!
    
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var taxTotalLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    
    var reportArray = [NSDictionary]()
    var summery = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentPicker.dataSource = self
        paymentPicker.delegate = self
        paymentTextFiled.inputView = paymentPicker
        viewPayment()
        //createDatePicker()

        
        
        
        datePickerStartDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerStartDate?.minimumDate = nil;
        datePickerStartDate?.maximumDate = Date();
        datePickerStartDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerStartDate?.datePickerMode = .date
        
        datePickerStartDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        
        startTextField?.inputView = datePickerStartDate
        startTextField.delegate = self
        startTextField?.inputAccessoryView = self.toolBarWithNext(target: self, action: #selector(barButtonNextAction))
        
        
        datePickerEndDate = UIDatePicker(frame: CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:200))
        datePickerEndDate?.minimumDate = nil;
        datePickerEndDate?.maximumDate = Date();
        datePickerEndDate?.translatesAutoresizingMaskIntoConstraints = false
        datePickerEndDate?.addTarget(self, action: #selector(datePickerValueChanged(datePicker:)), for: .valueChanged)
        datePickerEndDate?.datePickerMode = .date
        endTextField?.inputView = datePickerEndDate
        endTextField.delegate = self
        endTextField?.inputAccessoryView = self.toolbar(barButtonItem: UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(barButtonDoneAction)))
        
        datePickerValueChanged(datePicker: datePickerStartDate!)
        datePickerValueChanged(datePicker: datePickerEndDate!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return payment.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let name = payment[row]["name"] as? String  {
            if name == "Select"{
                return "All"
            }
            return name
        }
        else {
            return "Nil"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        if  payment[row]["name"] as! String == "Select" {
           paymentTextFiled.text = "All"
            paymentID = payment[row]["id"] as! String

        }else {
        paymentTextFiled.text = payment[row]["name"] as! String
       
        paymentID = payment[row]["id"] as! String
        }
       // print(paymentID!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @objc func datePickerValueChanged(datePicker:UIDatePicker) {
        if datePicker == datePickerStartDate {
            startTextField?.text = self.webServiceOnlyStringDateFromDate(date: datePickerStartDate!.date)
        } else if datePicker == datePickerEndDate {
            endTextField?.text = self.webServiceOnlyStringDateFromDate(date: datePickerEndDate!.date)
        }
    }
    
    func toolbar(barButtonItem: UIBarButtonItem) -> UIToolbar {
        
        let toolbar = UIToolbar()
        
        toolbar.barStyle = .default
        toolbar.frame = CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:40)
        
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),barButtonItem]
        return toolbar
    }
    @objc func barButtonNextAction() {
        startTextField?.resignFirstResponder()
        endTextField?.becomeFirstResponder()
    }
    
    @objc func barButtonDoneAction() {
        startTextField?.resignFirstResponder()
        endTextField?.resignFirstResponder()
    }
    
    func toolBarWithNext(target:UIViewController,action:Selector) -> UIToolbar {
        let barButtonNext = UIBarButtonItem(title: "Next", style: .done, target: target, action:action)
        
        let toolbarWithNext = self.toolbar(barButtonItem: barButtonNext)
        return toolbarWithNext
    }
    
    func webServiceOnlyStringDateFromDate(date:Date) -> String{
        //        let dateformatter = DateFormatter()
        
        let dateFormatter = DateFormatter()//2017-12-14 15:04:55
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //        dateFormatter.timeStyle = .none
        let stringDate = dateFormatter.string(from: date)
        return stringDate
        //        let date = dateFormatter.date(from: stringOnlyDate)
        //        return date!
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = reportTableView.dequeueReusableCell(withIdentifier: "PSReportTableViewCell") as! PSReportTableViewCell
        
        cell.dateLabel.text = reportArray[indexPath.row]["order_dtm"] as! String
        
        cell.salesIDLabel.text = reportArray[indexPath.row]["order_id"] as! String
        
        cell.paymentLabel.text = reportArray[indexPath.row]["payment_method_name"] as! String
        
        cell.subLabel.text = reportArray[indexPath.row]["subTotal"] as! String
        
        cell.taxLabel.text = reportArray[indexPath.row]["tax"] as! String
        
        cell.grandLabel.text = reportArray[indexPath.row]["grandTotal"] as! String

        return cell
        
    }
    
    
    
    
    func viewPayment(){
        
        let url = PSCommon.Global.baseURL+"settings/payment"
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.payment = dict
                          
                            self.paymentPicker.reloadInputViews()
                            
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }

    
    
    func viewReport(){
       // print(paymentID!)
       
        self.reportArray.removeAll();
        
        self.reportTableView.reloadData()
        let url = PSCommon.Global.baseURL+"report/sales"
        ProgressHUD.show()
        var body: Dictionary<String,AnyObject>?
        
        body = ["outlet":"1", "paid": paymentID!, "start_date":startTextField.text!, "end_date":endTextField.text!] as [String:AnyObject]
        
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
               
             //   print(json)
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"]  as? NSDictionary {
                            if let arrayDict  = dict["report"] as? [NSDictionary]{
          
                                self.reportArray = arrayDict
                                 self.reportTableView.reloadData()
                            }
                            
                            if let sum = dict["summery"] as? NSDictionary{
                               // print(arrayDict)
                              // self.summery = arrayDict
                                
                                if let t = sum["tax_amt"] {
                                    
                                    self.taxTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                if let t = sum["sub_amt"] {
                                    
                                    self.subTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                if let t = sum["grand_amt"] {
                                    
                                    self.grandTotalLabel.text = "\(t)"//" as? String
                                    
                                }
                                
                            }

                        }
                    }
                }else{
                    
                    
                    self.reportArray.removeAll();
                    
                    self.reportTableView.reloadData()
                    
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }

    @IBAction func getReportButtonPressed(_ sender: UIButton) {
        viewReport()
        
    }
    
    
    @IBAction func printSalesButtonPressed(_ sender: UIButton) {
        
        viewSales()
    }
    
    
    func viewSales(){
        // print(paymentID!)
    
        let url = PSCommon.Global.baseURL+"report/todaysale"
        ProgressHUD.show()
        var body: Dictionary<String,AnyObject>?
        
        body = ["outlet":"1", "start_date":startTextField.text!, "end_date":endTextField.text!] as [String:AnyObject]
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
               print(json)
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"]  as? NSDictionary {
                           
                            if let t = dict["totalamt"] {
                                
                                self.totalAmount = "\(t)"//" as? String
                                
                            }
                            if let t = dict["totalcash"] {
                                
                                self.totalCash = "\(t)"//" as? String
                                
                            }
                            if let t = dict["totalnett"] {
                                
                                self.totalNett = "\(t)"//" as? String
                                
                            }
                            
                            self.rePrint()
                        }
                    }
                }else{
                    
                    
                    self.reportArray.removeAll();
                    
                    self.reportTableView.reloadData()
                    
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
        
    }
    
    
    func rePrint(){
        var printData:String = ""
        var addString : String = ""
        // printData = "SKU         Description    Total\n"
        
        //printData = printData + "SKU         Description    Discount    Total\n"
        /*  for cart in orderDetail {
         //print(cart.productCode!+"\n")
         addString = ""
         addString = "\(cart["pcode"]!)"
         addString = addString.padding(toLength: 12, withPad: " ", startingAt: 0)
         printData = printData + addString
         addString = ""
         addString = cart["name"] as! String
         addString = addString.padding(toLength: 15, withPad: " ", startingAt: 0)
         printData = printData + addString
         addString = ""
         addString = cart["discount"] as! String
         addString = addString.padding(toLength: 12, withPad: " ", startingAt: 0)
         printData = printData + addString
         addString = ""
         addString = cart["price"] as! String
         addString = addString.padding(toLength: 10, withPad: " ", startingAt: 0)
         printData = printData + addString
         
         printData = printData + "\n"
         
         }*/
        printData = printData + "\n"
        
        addString = "Total Cash                    "
        printData = printData + addString
        printData = printData+"\(totalCash)"+"\n"
        addString = "Total Enets                   "
        printData = printData + addString
        printData = printData+"\(totalNett)"+"\n"
        /* addString = "Tax                         "
         printData = printData+addString
         printData = printData+"\(order["tax"]!)"+"\n"*/
        printData = printData+"--------------------------------\n"
        addString = "Total Amount                 "
        printData = printData+addString
        printData = printData+"\(totalAmount)"
        printData = printData+"\n"
        
        // print(printData)
        
        let commands: Data
        let emulation: StarIoExtEmulation = AppDelegate.getEmulation()
        
        let width: Int = AppDelegate.getSelectedPaperSize().rawValue
        
        let localizeReceipts: ILocalizeReceipts = LocalizeReceipts.createLocalizeReceipts(AppDelegate.getSelectedLanguage(), paperSizeIndex: AppDelegate.getSelectedPaperSize())
        
        if AppDelegate.getSelectedLanguage() != LanguageIndex.cjkUnifiedIdeograph {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: false, printData: printData)
            
        }
        else {
            commands = PrinterFunctions.createTextReceiptData(emulation, localizeReceipts: localizeReceipts, utf8: true,  printData: printData)
        }
        
        self.blind = true
        
        let portName: String = AppDelegate.getPortName()
        let portSettings: String = AppDelegate.getPortSettings()
        
        
        
        GlobalQueueManager.shared.serialQueue.async {
            _ = Communication.sendCommands(commands,
                                           portName: portName,
                                           portSettings: portSettings,
                                           timeout: 10000,  // 10000mS!!!
                completionHandler: { (result: Bool, title: String, message: String) in
                    DispatchQueue.main.async {
                        let alertView: UIAlertView = UIAlertView(title: title,
                                                                 message: message,
                                                                 delegate: nil,
                                                                 cancelButtonTitle: "OK")
                        
                            alertView.show()
                        
                        self.blind = false
                    }
            })
        }
        
        
        
        
        
        
    }
    
    
}
