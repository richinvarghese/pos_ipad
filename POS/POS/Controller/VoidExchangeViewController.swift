//
//  VoidExchangeViewController.swift
//  POS
//
//  Created by Richin Varghese on 04/10/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class VoidExchangeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var voidTableView: UITableView!
    var result = PSAPIClient()
    var transactionArray = [NSDictionary]()
    override func viewDidLoad() {
        super.viewDidLoad()
         let parms = ["outlet":"1", "exchange_status": "1"] as [String:AnyObject]
        PSAPIClient.SalesListByStatus(params: parms) { (response, message, success) in
            if(success){
                self.result = response as! PSAPIClient
                if let s = self.result.transactionDictionary["message"] {
                    if s as! String == "success"{
                        if let res =  self.result.transactionDictionary["response"] {
                            let r = res as! [String:Any]
                            
                            if let t = r["report"] as? [NSDictionary] {
                                
                                self.transactionArray = t
                                self.voidTableView.reloadData()
                               // print(self.transactionArray[1]["grandTotal"])
                            }
                            //self.transactionArray = res["report"] as! NSDictionary
                            
                        }
                     
                    }
                    
                    else {
                        print("error")
                    }
                }
                
                
            }
           
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = voidTableView.dequeueReusableCell(withIdentifier: "PSVoidExchangeTableViewCell") as! PSVoidExchangeTableViewCell
        cell.dateLabel.text = transactionArray[indexPath.row]["order_dtm"] as! String
        cell.saleIdLabel.text = transactionArray[indexPath.row]["order_id"] as! String
        cell.subTotalLabel.text = transactionArray[indexPath.row]["subTotal"] as! String
        cell.grandTotalLabel.text = transactionArray[indexPath.row]["grandTotal"] as! String

        cell.statusLabel.text = transactionArray[indexPath.row]["status"] as! String
        
        cell.showObject = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSShowOrderViewController") as! PSShowOrderViewController
            if let id   = self.transactionArray[indexPath.row]["order_id"] as? String{
               // print(id)
                vc.order_id = id
                self.showDetailViewController(vc, sender: self)
            }
            
        }

        return cell
    }
    

}
