//
//  PSPopUpViewController.swift
//  POS
//
//  Created by Richin Varghese on 16/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSPopUpViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   
    @IBOutlet weak var payebleTextField: UITextField!
    @IBOutlet weak var payableStackView: UIStackView!
    var tempValue : Double = 0
    @IBOutlet weak var paymentLabel: UILabel!
    
    @IBOutlet weak var returnLabel: UILabel!
    var payment = [NSDictionary]()
    var paymentPicker = UIPickerView()
    
    @IBOutlet weak var paymentTextField: UITextField!
    var addToCart = [PSProduct]()
    var order = PSOrder()
    var deleteProduct = [PSDeleteProduct]()
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    @IBOutlet weak var totalItemLabel: UILabel!
    
    @IBOutlet weak var paidAmountTextField: UITextField!
    @IBOutlet weak var returnChargeLabel: UILabel!
    var qty = 0
    var subTotal = 0.0
     var returnChange = 0.0
    var produtCode = [String]()
    var productPrice = [String]()
    var productQty = [Int]()
    var paymentID: String? = "1"
    var discounInd = [String]()
    
    
    var delProductCode = [String]()
    var delProductPrice = [String]()
    var delProductQty = [Int]()
    var delDiscounInd = [String]()
    
    @IBOutlet weak var customerNameTextField: UITextField!
    
    @IBOutlet weak var paidAmountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(order.)
        payableStackView.isHidden = true
        if(order.salesID != ""  && order.voidStatus ==  true) {
            
          //  returnLabel.text = "Refund Amount"
            paidAmountLabel.text = "Amount Paid"
            
            paidAmountTextField.text = "\(String(order.grandTotal!))"
           // returnLabel.text = "Return/"
             order.returnCharge = paidAmountTextField.text
            returnLabel.text = " Change Due"
            returnChargeLabel.text =  paidAmountTextField.text
            paidAmountTextField.isUserInteractionEnabled = false
            order.paidAmount =  paidAmountTextField.text

            
        }
        else if (order.salesID != "" && order.voidStatus == false){
            //paidAmountTextField.text = "\(String(order.grandTotal!))"
           // returnLabel.text = "0.0"
            returnChargeLabel.text = "0.0"
            order.returnCharge = "0"
              paidAmountLabel.text = "Amount Paid"
            
        }
        paymentPicker.dataSource = self
        paymentPicker.delegate = self
        paymentTextField.inputView = paymentPicker
        viewPayment()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.showAnimate()
        //print(addToCart.count)
        var sum = 0
        
        var price = 0.0
        var totalPrice = 0.0
       // print("sub \(order.subTotal)")
        subTotal = order.subTotal!
        customerNameTextField.text = order.customer
        for item in addToCart {
            
                produtCode.append(item.productCode!)
            productPrice.append(item.price!)
            productQty.append(item.qty!)
            discounInd.append("\(item.discount!)")
            

            //sum += Double(item.price)! * Double(item.qty)
            
            if let q = item.qty {
                

                qty +=  q
            }
            if let p = item.price {
                price = (p as NSString).doubleValue
            }
           // subTotal += Double(qty) * price
            
        }
       // var g = order.grandTota
        totalItemLabel.text = "\(qty)"
        totalAmountLabel.text = "\(String(order.grandTotal!))"
        if let gd = order.tempGrandTotal{
             if(order.salesID != ""  && order.voidStatus ==  false) {
            tempValue = order.grandTotal! - gd
            //totalAmountLabel.text = "\(String(tempValue))"
            paidAmountTextField.text = "\(String(abs(tempValue)))"
                order.paidAmount =  paidAmountTextField.text
                if(tempValue<0){
                    returnLabel.text = "Change Due"
                    paidAmountTextField.isUserInteractionEnabled = false
                    returnChargeLabel.text = paidAmountTextField.text
                     payableStackView.isHidden = true
                }
                else if(tempValue>0){
                    payableStackView.isHidden = false
                   // paidAmountLabel.text = "Balance Due(Retailer)"
                    returnLabel.text = "Balance Due"
                    returnChargeLabel.text = order.paidAmount
                    payebleTextField.text = order.paidAmount
                    order.returnCharge = returnChargeLabel.text
                    //paidAmountTextField.isUserInteractionEnabled = false
                    
                }
                else {
                   returnLabel.text = "Balance Due"
                }
            }

        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func submitButtonPressed(_ sender: Any) {
        if let r = order.returnCharge{
            let retValue = (r as NSString).doubleValue
            if qty > 0 {
                insertSales();
                print(order.paidAmount)
                removeAnimate()
            }
            else {
                let alertController = UIAlertController(title: "Error", message:
                    "Please enter amount paid", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
       // removeAnimate()
        // self.view.removeFromSuperview()
    }
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        })
        

    }
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
            })
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished: Bool) in
            if (finished) {
                self.view.removeFromSuperview()
                
                if self.order.voidStatus == false {
                    
                 let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSInvoiceViewController") as! PSInvoiceViewController
                    vc.addToCart =  self.addToCart
                    vc.order = self.order
                    self.showDetailViewController(vc, sender: self)
                }
                else {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSInvoiceViewController") as! PSInvoiceViewController
                    vc.addToCart =  self.addToCart
                    vc.order = self.order
                    self.showDetailViewController(vc, sender: self)
                    
              //  let  vc = self.storyboard?.instantiateViewController(withIdentifier: "PSTransactionViewController") as! PSTransactionViewController
                //    self.showDetailViewController(vc, sender: self)
                }
                
               
                //self.navigationController?.show(vc, sender: self)
        }
        
        
    })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func amoutPaidButton(_ sender: UITextField) {
        //print(paidAmountLabel)
        if(order.salesID == ""  && order.voidStatus ==  false) {
            let grand = order.grandTotal
            var pamount = 0.0
           
            if let p =  paidAmountTextField.text {
             pamount = (p as NSString).doubleValue
            }
           
            order.paidAmount = paidAmountTextField.text
            returnChange = pamount - grand!
            returnChargeLabel.text = "\(returnChange)"
            order.returnCharge = returnChargeLabel.text
            
        }
        else if(order.salesID != ""  && order.voidStatus ==  false) {
            //returnChange =
            if let gd = order.tempGrandTotal{
                var pamount = 0.0
                if let p =  paidAmountTextField.text {
                    pamount = (p as NSString).doubleValue
                }
                    tempValue = order.grandTotal! - gd
                returnChange = (abs(tempValue - pamount))
                returnChargeLabel.text = "\(returnChange)"
                order.returnCharge = "\(returnChange)"
                order.paidAmount = paidAmountTextField.text
                
                //totalAmountLabel.text = "\(String(tempValue))"
                   // paidAmountTextField.text = "\(String(abs(tempValue)))"
                    /*w if(tempValue<0){
                     paidAmountLabel.text = "Balance Due(Customer)"
                     }
                     else if(tempValue>0){
                     paidAmountLabel.text = "Balance Due(Retailer)"
                     }
                     else {
                     paidAmountLabel.text = "Balance Due"
                     }*/
                
                
            }
        }
        else if(order.salesID != ""  && order.voidStatus ==  true) {
            if let gd = order.tempGrandTotal{
                var pamount = 0.0
                if let p =  paidAmountTextField.text {
                    pamount = (p as NSString).doubleValue
                }
                tempValue = pamount - order.grandTotal!
                returnChange = (abs(tempValue))
                returnChargeLabel.text = "\(returnChange)"
        }
        }
    }
    func insertSales(){
       
        let dicount = order.discountAmt!
        var body: Dictionary<String,AnyObject>?
        var url = ""
        
        if(order.salesID == "" &&  order.voidStatus == false) {
            url = PSCommon.Global.baseURL+"pos/sales_insert"
            body = ["addi_card_numb":"", "card_numb":"", "subTotal":subTotal, "dis_amt":dicount,"final_total_payable":order.grandTotal!, "final_total_qty": qty,
                    "tax_amt": order.taxTotal!,
                    "customer": order.customerId! , "paid_by":paymentID, "cheque":"","paid": order.paidAmount!,
                    "returned_change":returnChange, "user_id": "1", "outlet":"1", "pcode":produtCode, "price": productPrice, "qty": productQty, "discountind":discounInd] as [String:AnyObject]
        }
        else if (order.salesID != "" && order.voidStatus == false){
             //let tempValue = order.grandTotal! - gd
            
            for item in deleteProduct {
                
                delProductCode.append(item.productCode!)
                delProductPrice.append(item.price!)
                delProductQty.append(item.qty!)
                delDiscounInd.append("\(item.discount!)")
            }
        url = PSCommon.Global.baseURL+"pos/insertexchange"
            body = ["parent_order_id":order.salesID, "customer":order.customerId, "outlet":"1",
                     "refund_amt":order.subTotal!, "refund_tax": order.tax,
                     "refund_by": paymentID, "cheque_numb":"","refund_method": "2", "refund_grand":order.grandTotal!,
                    "row_count": productQty.count+1, "remark":"",
                    "user_id": "1",  "pcode":produtCode, "price": productPrice, "qty": productQty, "discountind":discounInd, "final_total_qty": qty,
                     "delete_pcode": delProductCode,
                     "delete_price":delProductPrice,
                     "delete_qty":delProductQty,
                     "delete_dis":delDiscounInd,
                     "gt":tempValue
                     ] as [String:AnyObject]
        }
        else if(order.salesID != ""  && order.voidStatus == true) {
            url = PSCommon.Global.baseURL+"pos/insertReturnOrder"
            body = ["parent_order_id":order.salesID, "customer":order.customerId, "outlet":"1",
                    "refund_amt":order.subTotal!, "refund_tax": order.tax,
                    "refund_by": paymentID, "cheque_numb":"","refund_method": "1", "refund_grand":order.grandTotal!,
                    "row_count": productQty.count+1, "remark":"", "returned_change":returnChange,
                    "user_id": "1",  "pcode":produtCode, "price": productPrice, "qty": productQty, "final_total_qty": qty
                    ] as [String:AnyObject]
        }
        
        
   
        ProgressHUD.show()
        let header = ["Content-Type": "application/json",
                      "accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"] == "success" {
                    
                    print(json["message"])
                    self.order.salesID = "\(json["message"])"
                    
                    
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
        showDetailViewController(vc, sender: self)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return payment.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let name = payment[row]["name"] as? String  {
            return name
        }
        else {
            return "Nil"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        paymentTextField.text = payment[row]["name"] as! String
        paymentID = payment[row]["id"] as! String
    }
    func viewPayment(){
        
        let url = PSCommon.Global.baseURL+"settings/payment"
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
              //  print(json)
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.payment = dict
                          //  print(self.payment)
                            self.paymentPicker.reloadInputViews()
                            
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    
    
}
