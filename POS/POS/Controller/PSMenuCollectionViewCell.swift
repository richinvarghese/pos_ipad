//
//  PSMenuCollectionViewCell.swift
//  POS
//
//  Created by Richin Varghese on 31/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuImageView: UIImageView!
    
    
    @IBOutlet weak var menuLabel: UILabel!
    
}
