//
//  PSMenuViewController.swift
//  POS
//
//  Created by Richin Varghese on 31/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSMenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    


    @IBOutlet weak var menuCollectionView: UICollectionView!
    let menu = ["Dashboard", "Customers", "Gift Card", "Sales", "Reports", "Point Of Sales", "Inventory", "Products", "System Setting"]
    
    let menuImages: [UIImage] = [
        UIImage(named: "IC_Dashboard")!,
        UIImage(named: "IC_Customer")!,
        UIImage(named: "IC_Gift")!,
        UIImage(named: "IC_Sales")!,
        UIImage(named: "IC_Report")!,
        UIImage(named: "IC_POS")!,
        UIImage(named: "IC_Inventory")!,
        UIImage(named: "IC_Product")!,
        UIImage(named: "IC_Setting")!
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        menuCollectionView.delegate = self
        menuCollectionView.dataSource = self
        
        var layout  = self.menuCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumInteritemSpacing = 4
    layout.itemSize = CGSize(width: (self.menuCollectionView.frame.size.width - 40), height: self.menuCollectionView.frame.size.height / 12)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeMenuButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = menuCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PSMenuCollectionViewCell
    
        cell.menuImageView .image = menuImages[indexPath.item]
        cell.menuLabel.text = menu[indexPath.item]
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        //     cell?.layer.borderColor = UIColor(red: 247/255.0, green: 49/255.0, blue: 72/255.0, alpha: 1).cgColor
        let color1 = UIColor(hexString: "#F73148")
        cell?.layer.borderColor = color1.cgColor
        cell?.layer.borderWidth = 2
        
        switch indexPath.row {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSDashboardViewController") as! PSDashboardViewController
            navigationController?.show(vc, sender: self)
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSCustomerViewController")
                as! PSCustomerViewController
            navigationController?.show(vc, sender: self)
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSGiftCardViewController")
                as! PSGiftCardViewController
            navigationController?.show(vc, sender: self)
        case 5:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
            navigationController?.show(vc, sender: self)
        default:
            print("Under Testing")
        }
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.darkGray.cgColor
        cell?.layer.borderWidth = 0.5
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
