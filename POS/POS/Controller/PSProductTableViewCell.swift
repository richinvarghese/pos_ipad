//
//  PSProductTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 07/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSProductTableViewCell: UITableViewCell {

    @IBOutlet weak var codeLabel: UILabel!
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    var editObject : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func editButtonPressed(_ sender: UIButton) {
        if let btnAction = self.editObject {
            btnAction()
        }
    }
    
    @IBAction func printBarcodeButtonPressed(_ sender: UIButton) {
    }
    
}
