//
//  PSInventoryViewController.swift
//  POS
//
//  Created by Richin Varghese on 22/08/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire

class PSInventoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    var inventory = [NSDictionary]()
    @IBOutlet weak var inventoryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewInventory()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inventory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = inventoryTableView.dequeueReusableCell(withIdentifier: "PSInventoryTableViewCell") as! PSInventoryTableViewCell
        cell.codeLabel.text = inventory[indexPath.row]["code"] as! String
        cell.nameLabel.text = inventory[indexPath.row]["name"] as! String
        cell.categoryLabel.text = inventory[indexPath.row]["catname"] as! String
        if (inventory[indexPath.row]["qty"] is NSNull){
              cell.qtyLabel.text =  "0"
        }else{
            cell.qtyLabel.text =  inventory[indexPath.row]["qty"] as! String
        }
        
        /*if let qty = inventory[indexPath.row]["qty"] as! String {
            if (qty == "null" ) {
                cell.qtyLabel.text =  "0"
            }
            else {
                cell.qtyLabel.text =  qty as! String
            }
 
        }
        else{
        cell.qtyLabel.text =  "0"
        }*/
        return cell
        
    }
    
    func viewInventory(){
        
        let url = PSCommon.Global.baseURL+"inventory/view"
        ProgressHUD.show()
       // var body: Dictionary<String,AnyObject>?
        
        //body = ["outlet":"1", "paid": paymentID, "start_date":startTextField.text!, "end_date":endTextField.text!] as [String:AnyObject]
        
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                //let json = JSON(value)
                
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.inventory = dict
                            
                            self.inventoryTableView.reloadData()
                            
                        }
                    }
                }else{
                    
                    let alertController = UIAlertController(title: "Error", message:
                        "Product List is Empty", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }

}
