//
//  PSDashboardCollectionViewCell.swift
//  POS
//
//  Created by Richin Varghese on 27/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSDashboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuNameLabel: UILabel!
    @IBOutlet weak var dashboardMenuImageView: UIImageView!
}
