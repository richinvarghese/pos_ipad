//
//  PSTodaysSalesTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 22/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSTodaysSalesTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var salesIDLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
