//
//  PSSalesHistoryTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 20/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSSalesHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var saleIdLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var totalQty: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
