//
//  PSCustomerViewController.swift
//  POS
//
//  Created by Richin Varghese on 04/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSCustomerViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var customerTableView: UITableView!
    
    
    var arrayOfCustomers = [PSCustomer]()
    var customerArray = [NSDictionary]()
    var customerId: String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
       
        

        // Do any additional setup after loading the view.
        //run
        
        
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
       // customerTableView.delegate = self
        
       // customerTableView.delegate = self
           viewCustomer()
     //   self.tableView.tableHeaderView =
       // customerTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    }
      //  customerTableView.remo
    
       
        
    //cll me..
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return customerArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = customerTableView.dequeueReusableCell(withIdentifier: "PSCustomerTableViewCell") as! PSCustomerTableViewCell
        if let name = customerArray[indexPath.row]["fullname"] as? String{
         cell.nameLabel!.text = name
        }
        else{
            cell.nameLabel!.text = "Nil"
        }
        if let email = customerArray[indexPath.row]["email"] as? String{
            cell.emailLabel!.text = email
        }
        if let mobile   = customerArray[indexPath.row]["mobile"] as? String{
            cell.mobileLabel!.text = mobile
        }
        cell.historyObject =
            {
              let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSSalesHistoryViewController") as! PSSalesHistoryViewController
                if let id   = self.customerArray[indexPath.row]["id"] as? String{
                   vc.customerId = id
                    self.showDetailViewController(vc, sender: self)
                }
                
        }
        cell.editObject = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSAddCustomerViewController") as! PSAddCustomerViewController
            if let id   = self.customerArray[indexPath.row]["id"] as? String{
                vc.customerId = id
               self.showDetailViewController(vc, sender: self)
            }
            
        }
        
 
        return cell
    }
 
    @IBAction func addCustomerButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSAddCustomerViewController") as! PSAddCustomerViewController
        showDetailViewController(vc, sender: self)
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
  
    
    func viewCustomer(){
     
         let url = PSCommon.Global.baseURL+"customers/view"
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                //your success code
                // print(response)
                let json = JSON(value)
               // print("JSON: \(json)")
              //  print(response.result.value)
                
                   
                    if let resultDict = response.result.value as? NSDictionary {
                        if resultDict["status"]as! String  == "success" {

                    if let dict = resultDict["message"] as? [NSDictionary] {
                        self.customerArray = dict
                       // print(self.customerArray)
                         self.customerTableView.reloadData()
                        
                
                    
                    self.customerTableView.reloadData()
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    
    


}
