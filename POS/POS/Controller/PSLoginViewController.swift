//
//  PSLoginViewController.swift
//  POS
//
//  Created by Richin Varghese on 26/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON
import SDWebImage


class PSLoginViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var userTextField: UITextField!
    
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
      //print(  StarPrinterStatus_.init())
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        getAddress()
        
     //  refreshDeviceStatus()
        
        

        loginButton.layer.cornerRadius = 5.0
        navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        userTextField.text = "" //"vimalkarthik@apstrix.com"
        passwordTextField.text = "" //"password"
        
        
        
        let string = UserDefaults.standard.object(forKey: "logo")
        var url = URL(string: PSCommon.Global.baseURL)
        if let u = string  {
              url = URL(string: string as! String)
        }
        
       // print(url)
        logoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        
      //  cell.productImage?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
        
    }

    @IBAction func loginAction(_ sender: UIButton) {
        guard(whitespaceValidation(userTextField.text!) == true)
            else
        {
            return alert(alertmessage: "Email should not be blank")
        }
        guard(whitespaceValidation(passwordTextField.text!) == true)
            else
        {
            return alert(alertmessage: "Password should not be blank")
        
        
        }
        login(user: userTextField.text!, password: passwordTextField.text!)
    
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSDashboardViewController")
            as! PSDashboardViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

func refreshDeviceStatus() {
    var result: Bool = false
    /*
    self.blind = true
    
    defer {
        self.blind = false
    }*/
    
    
    var error: NSError?
    
    while true {
        let portName = AppDelegate.getPortName()
        guard let port: SMPort = SMPort.getPort(portName, AppDelegate.getPortSettings(), 10000) else {     // 10000mS!!!
            break
        }
        
        defer {
            SMPort.release(port)
        }
        
        // Sleep to avoid a problem which sometimes cannot communicate with Bluetooth.
        // (Refer Readme for details)
        if #available(iOS 11.0, *), portName.uppercased().hasPrefix("BT:") {
            Thread.sleep(forTimeInterval: 0.2)
        }
        
        var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
        
        port.getParsedStatus(&printerStatus, 2, &error)
        
        if error != nil {
            break
        }
        
        if printerStatus.offline == sm_true {
          //  self.statusCellArray.add(["Online", "Offline", UIColor.red])
            print("Offline")
            
        }
        else {
           // self.statusCellArray.add(["Online",  "Online",  UIColor.blue])
            print("Online")
        }
        
        if printerStatus.coverOpen == sm_true {
           // self.statusCellArray.add(["Online", "Open",   UIColor.red])
            print("Cover - Open")
        }
        else {
           // self.statusCellArray.add(["Cover", "Closed", UIColor.blue])
            print("Cover - Closed")
        }
        
        if printerStatus.receiptPaperEmpty == sm_true {
         //   self.statusCellArray.add(["Paper", "Empty", UIColor.red])
            print("Paper - Empty")
        }
        else if printerStatus.receiptPaperNearEmptyInner == sm_true ||
            printerStatus.receiptPaperNearEmptyOuter == sm_true {
           // self.statusCellArray.add(["Paper", "Near Empty", UIColor.orange])
            print("Paper - Near Empty")
        }
        else {
            //self.statusCellArray.add(["Paper", "Ready",      UIColor.blue])
            print("Paper - Ready")
        }
        
        if AppDelegate.getCashDrawerOpenActiveHigh() == true {
            if printerStatus.compulsionSwitch == sm_true {
                //self.statusCellArray.add(["Cash Drawer", "Open",   UIColor.red])
                print("Cash Drawer - Open")
            }
            else {
             //   self.statusCellArray.add(["Cash Drawer", "Closed", UIColor.blue])
                print("Cash Drawer - Closed")
            }
        }
        else {
            if printerStatus.compulsionSwitch == sm_true {
                //self.statusCellArray.add(["Cash Drawer", "Closed", UIColor.blue])
                print("Cash Drawer - Closed")
            }
            else {
               // self.statusCellArray.add(["Cash Drawer", "Open",   UIColor.red])
                print("Cash Drawer - Open")
            }
        }
        
        if printerStatus.overTemp == sm_true {
            //self.statusCellArray.add(["Head Temperature", "High",   UIColor.red])
            print("Head Temperature - High")
        }
        else {
            //self.statusCellArray.add(["Head Temperature", "Normal", UIColor.blue])
            print("Head Temperature - Normal")
        }
        
        if printerStatus.unrecoverableError == sm_true {
          //  self.statusCellArray.add(["Non Recoverable Error", "Occurs", UIColor.red])
            print("on Recoverable Error -  Occurs")
        }
        else {
            //self.statusCellArray.add(["Non Recoverable Error", "Ready",  UIColor.blue])
            print("on Recoverable Error -  Ready")
        }
        
        if printerStatus.cutterError == sm_true {
         //   self.statusCellArray.add(["Paper Cutter", "Error", UIColor.red])
            print("Paper Cutter - Error")
        }
        else {
           // self.statusCellArray.add(["Paper Cutter", "Ready", UIColor.blue])
            print("Paper Cutter - Ready")
        }
        
        if printerStatus.headThermistorError == sm_true {
            //self.statusCellArray.add(["Head Thermistor", "Error",  UIColor.red])
            print("Head Thermistor - Error")
        }
        else {
            //self.statusCellArray.add(["Head Thermistor", "Normal", UIColor.blue])
            print("Head Thermistor - Normal")
        }
        
        if printerStatus.voltageError == sm_true {
           // self.statusCellArray.add(["Voltage", "Error",  UIColor.red])
            print("Voltage - Errors")
        }
        else {
           // self.statusCellArray.add(["Voltage", "Normal", UIColor.blue])
            print("Voltage Normal")
        }
        
        if printerStatus.etbAvailable == sm_true {
            //self.statusCellArray.add(["ETB Counter", String(format: "%d", printerStatus.etbCounter), UIColor.blue])
            print("ETB Counter - "+String(format: "%d", printerStatus.etbCounter))
        }
        
        if printerStatus.offline == sm_true {
         //   self.firmwareInfoCellArray.add(["Unable to get F/W info. from an error.", "", UIColor.red])
            print("Unable to get F/W info. from an error")
            
            result = true
            break
        }
        else {
            let dictionary: Dictionary = port.getFirmwareInformation(&error)
            
            if error != nil {
                break
            }
            
            let modelName:       String = dictionary["ModelName"]       as! String
            let firmwareVersion: String = dictionary["FirmwareVersion"] as! String
            print("modelName - firmwareVersion ")
         //   self.firmwareInfoCellArray.add(["Model Name",       modelName,       UIColor.blue])
           // self.firmwareInfoCellArray.add(["Firmware Version", firmwareVersion, UIColor.blue])
            
            result = true
            break
        }
    }
    
    if result == false {
        let alertView: UIAlertView = UIAlertView.init(title: "Fail to Open Port", message: "", delegate: nil, cancelButtonTitle: "OK")
        
        alertView.show()
    }
    
   // self.tableView.reloadData()
}


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func alert(alertmessage:String)
    {
        let alert = UIAlertController(title: alertmessage, message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func whitespaceValidation(_ name:String) -> Bool
    {
        let whitespaceSet = CharacterSet.whitespaces
        if !name.trimmingCharacters(in: whitespaceSet).isEmpty
        {
            return true
        }
        else
        {
            return false
        }
    }

    
    // MARK: - Login
    
    func login(user: String, password: String){
        let loginURL = PSCommon.Global.baseURL+"auth/login"
        
        var body: Dictionary<String,String>?
        
        body = ["username":user, "password": password] as [String:String]

        //Alamofire.request(loginURL, method: .post, parameters: ["" : ""], encoding: URLEncoding.default, headers: ["" : ""] ).authenticate(user: user, password: password)
        
        
       // let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        
       // let base64Credentials = credentialData.base64EncodedString()
        
        var dict : NSDictionary?
        /*let headers = [
            "Authorization": "Basic \(base64Credentials)",
            "Accept": "application/json",
            "Content-Type": "application/json" ]*/
        
        //put slash before \ (base64Credentials)
        ProgressHUD.show()
    /*    let progressHUD = ProgressHUD(text: "")
        
        self.view.addSubview(progressHUD)*/
    
        Alamofire.request(loginURL, method: .post, parameters: body,encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                //your success code
               // print(response)
                let json = JSON(value)
                //print("JSON: \(json)")
                if json["status"] == "success" {
                   // print(json["message"])
                   // let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSDashboardViewController") as! PSDashboardViewController
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSSplitViewController") as! PSSplitViewController
                    self.present(vc, animated: true, completion: nil);
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                       message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                 print(error)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func getAddress(){
        
        let url = PSCommon.Global.baseURL+"settings/address"
        ProgressHUD.show()
        var body: Dictionary<String,AnyObject>?
        
        body = ["outlet":"1"] as [String:AnyObject]
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                print(json)
                if let resultDict = response.result.value as? NSDictionary {
                    
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? NSDictionary  {
                            
                            print(dict)
                            if let t = dict["name"] {
                                
                              //  self.cname = "\(t)"//" as? String
                                UserDefaults.standard.set("\(t)", forKey: "cname"  )
                               // print(self.cname)
                            }
                            if let t = dict["address"] {
                                
                               // self.caddress = "\(t)"//" as? String
                                UserDefaults.standard.set("\(t)", forKey: "caddress" )
                               // print(self.caddress)
                                //let string = UserDefaults.standard.object(forKey: "caddress")
                               
                            }
                            
                            if let t = dict["contact_number"] {
                                
                                // self.caddress = "\(t)"//" as? String
                                UserDefaults.standard.set("\(t)", forKey: "cphone" )
                                // print(self.caddress)
                                //let string = UserDefaults.standard.object(forKey: "caddress")
                                
                            }
                            if let t = dict["receipt_footer"] {
                                
                             
                                UserDefaults.standard.set("\(t)", forKey: "footer" )
                               
                            }
                            if let t = dict["logo"] {
                                
                                 //   PSCommon.Global.base+"assets/img/logo/"+"\(t)"
                                var temp = PSCommon.Global.base+"assets/img/logo/"+"\(t)"
                                // self.caddress = "\(t)"//" as? String
                                UserDefaults.standard.set("\(temp)", forKey: "logo" )
                                // print(self.caddress)
                                //let string = UserDefaults.standard.object(forKey: "caddress")
                                
                            }
                            
                            //http://localhost/apstrix/pos/assets/img/logo/
                            
                        }
                        else{
                            
                        }
                    }
                }else{
                    
                    let alertController = UIAlertController(title: "Error", message:
                        "Loading Completed", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    // self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
        
    }

}
