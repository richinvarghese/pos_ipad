//
//  PSTransactionTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 04/09/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSTransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var salesIDLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    
    
    var editObject : (() -> Void)? = nil
    var voidObject : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    @IBAction func printButtonPressed(_ sender: UIButton) {
        if let btnAction = self.editObject {
            btnAction()
        }
    }
    
    @IBAction func voidButtonPressed(_ sender: UIButton) {
        if let btnAction = self.voidObject {
            btnAction()
        }
    }
}
