//
//  PSCartTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 15/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSCartTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
   
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var subtotalPriceLabel: UILabel!
    var addobj : (() -> Void)? = nil
    var minusobj : (() -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var qtyTextField: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func minusAction(_ sender: UIButton) {
        if let btnAction = self.minusobj
        {
            btnAction()
        }
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        if let btnAction = self.addobj
        {
            btnAction()
        }
    }

}
