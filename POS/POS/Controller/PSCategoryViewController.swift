//
//  PSCategoryViewController.swift
//  POS
//
//  Created by Richin Varghese on 10/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var categoryTableView: UITableView!
     var categoryArray = [NSDictionary]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCategory()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArray.count-1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  categoryTableView.dequeueReusableCell(withIdentifier: "PSCategoryTableViewCell") as! PSCategoryTableViewCell
        if let name = categoryArray[indexPath.row+1]["name"] as? String{
            cell.nameLabel!.text = name
        }
        else{
            cell.nameLabel!.text = "Nil"
        }
        if let status = categoryArray[indexPath.row+1]["status"] as? String{
            if status == "0" {
                cell.statusLabel!.text = "Inactive"
            }
            else {
                cell.statusLabel!.text = "Active"
            }
        }
        
        cell.editObject = {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSAddCategoryViewController") as! PSAddCategoryViewController
            if let id   = self.categoryArray[indexPath.row+1]["id"] as? String{
                vc.categoryId = id
                self.showDetailViewController(vc, sender: self)
            }
        }
       
        
        
        return cell
    }
    
    @IBAction func addCategoryButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSAddCategoryViewController")
            as! PSAddCategoryViewController
        showDetailViewController(vc, sender: self)
    }
    func viewCategory(){
        
        let url = PSCommon.Global.baseURL+"category/view"
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                print(json)
               
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.categoryArray = dict
                            print(self.categoryArray)
                            self.categoryTableView.reloadData()
                            
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    
}
