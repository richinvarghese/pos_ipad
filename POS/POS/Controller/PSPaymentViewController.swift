//
//  PSPaymentViewController.swift
//  POS
//
//  Created by Richin Varghese on 15/08/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSPaymentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var paymentTableView: UITableView!
     var paymentArray = [NSDictionary]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
         viewPayment()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return paymentArray.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  paymentTableView.dequeueReusableCell(withIdentifier: "PSPaymentTableViewCell") as! PSPaymentTableViewCell
        if let name = paymentArray[indexPath.row+1]["name"] as? String{
            cell.paymentLabel!.text = name
            
        }
        if let status = paymentArray[indexPath.row+1]["status"] as? String{
            if status == "1" {
            cell.statusLabel!.text = "Active"
            }
            else {
                 cell.statusLabel!.text = "Not Active"
            }
        }
        cell.editObject = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSPaymentUpdateViewController") as! PSPaymentUpdateViewController
            if let id   = self.paymentArray[indexPath.row+1]["id"] as? String{
                vc.paymentID = id
                vc.paymentName = cell.paymentLabel!.text
                vc.paymentStatus = self.paymentArray[indexPath.row+1]["status"] as? String
                self.showDetailViewController(vc, sender: self)
            }
        }
        
        return cell

    }
    
    func viewPayment(){
        
        let url = PSCommon.Global.baseURL+"settings/payment"
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
               let json = JSON(value)
                print(json)
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.paymentArray = dict
                            print(self.paymentArray)
                            self.paymentTableView.reloadData()
                            
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    

}
