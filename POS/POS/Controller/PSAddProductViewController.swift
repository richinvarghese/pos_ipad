//
//  PSAddProductViewController.swift
//  POS
//
//  Created by Richin Varghese on 07/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON
import SDWebImage

class PSAddProductViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imageCategory: UIImageView!
    
    @IBOutlet weak var codeTextField: UITextField!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var productCategoryTextField: UITextField!
    
    @IBOutlet weak var productPriceTextField: UITextField!
    
    @IBOutlet weak var retailPriceTextField: UITextField!
    var categoryId: String?
    var productId: String?
    
    var pickOption = [NSDictionary]()//["one", "two", "three", "seven", "fifteen"]
    override func viewDidLoad() {
        super.viewDidLoad()
        var pickerView = UIPickerView()
        
        pickerView.delegate = self
        viewCategory()
        productCategoryTextField.inputView = pickerView
        // Do any additional setup after loading the view.
        if productId != nil {
            getProduct()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let name = pickOption[row]["name"] as? String  {
            return name
        }
        else {
            return "Nil"
        }
      //  return "aa"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        productCategoryTextField.text =  pickOption[row]["name"] as? String
        categoryId = pickOption[row]["id"] as? String
    }
    func viewCategory(){
        
        let url = PSCommon.Global.baseURL+"category/view"
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                //print(json)
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.pickOption = dict
                         //   print(self.pickOption[1]["name"])
                            self.productCategoryTextField.reloadInputViews()
                            
                        }
                    }
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
   /* override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
   
*/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        productCategoryTextField.resignFirstResponder()
        return true
    }
    
    @IBAction func chooseImage(_ sender: UIButton) {
        
      //  self.chooseImage.setTitleColor(UIColor.white, for: .normal)
        //self.chooseImage.isUserInteractionEnabled = true
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
       /* let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        */
    }
   
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
             imagePicker.delegate = self
             imagePicker.modalPresentationStyle = .overCurrentContext
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .overCurrentContext
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
   @objc  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageCategory.image = image
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    

    
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        var compressedData: Data?
        
        if categoryId != nil {
        if imageCategory.image != nil {
            let imageCompressed = PSCommon.compressImage(image: imageCategory.image!)
            let image = UIImage(data:imageCompressed!)
            compressedData = UIImagePNGRepresentation(image!)
            postTimeLine(fileData: compressedData)
            //            compressedData = imageCompressed//?.base64EncodedData()
            //            compressedData.
            //            let strBase64:String = (imageCompressed?.base64EncodedString())!
            //            compressedData = strBase64.data(using: .utf8)
            //            print(compressedData)
        }
        }
        else{
          //  print("f")
            let alertController = UIAlertController(title: "Error", message:
                "Please fill all fields", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
           
        }
        
    }
//        func imageupload()
//        {
//            var compressedData: Data?
//
//            if imageCategory.image != nil {
//                let imageCompressed = PSCommon.compressImage(image: imageCategory.image!)
//                let image = UIImage(data:imageCompressed!)
//                compressedData = UIImagePNGRepresentation(image!)
//             let url = PSCommon.Global.baseURL+"products/insert"
//
//            let parameters = ["created_user_id":"1", "code": codeTextField.text!, "name": nameTextField.text!, "category":"1", "purchase": productPriceTextField.text!, "retail": retailPriceTextField.text!] as [String:String]
//
//            Alamofire.upload(multipartFormData: { multipartFormData in
//                if let imageData = UIImagePNGRepresentation(image!) {
//                    multipartFormData.append(imageData, withName: "uploadFile", fileName: "file.jpeg", mimeType: "image/jpeg")
//                }
//
//                for p in parameters {
//                    let value = p.value
//                    multipartFormData.append((value.data(using: .utf8))!, withName: p.key)
//                }}, to: url, method: .post, headers: nil,
//                    encodingCompletion: { encodingResult in
//                        switch encodingResult {
//                        case .success(let upload, _, _):
//                            upload.response { [weak self] response in
//                                guard let strongSelf = self else {
//                                    return
//                                }
//                                print(response.data)
//                                print("strongSelf")
//
//                                debugPrint(response)
//
//                            }
//                        case .failure(let encodingError):
//                            print("error:\(encodingError)")
//                        }
//            })
//    }
//    }
//        func imageUploadUrl()
//        {
//            let url = PSCommon.Global.baseURL+"products/insert"
//            let dicImgData : NSMutableDictionary? = NSMutableDictionary()
//            var compressedData: Data?
//
//            if imageCategory.image != nil {
//                let imageCompressed = PSCommon.compressImage(image: imageCategory.image!)
//                let image = UIImage(data:imageCompressed!)
//                compressedData = UIImagePNGRepresentation(image!)
//            //
//            //        if let img = profileImage.image {
//            //            if let data:Data = UIImagePNGRepresentation(img) {
//            //                let imageData : Data = data
//            //                dicImgData! .setObject(imageData, forKey: "data" as NSCopying)
//            //                dicImgData! .setObject("file", forKey: "name" as NSCopying)
//            //                dicImgData! .setObject("file.png", forKey: "fileName" as NSCopying)
//            //                dicImgData! .setObject("image/png", forKey: "type" as NSCopying)
//            //
//            //                let dicParameter = ["file1":"p_\(self.userReg!)","fileNames":"p_\(self.userReg!)","id":"\(userReg!)","userType":"5"]
//            //
//            //                self.uploadImage(url: "http://35.231.13.2:8080/bestdoc6_dev/webresources/image/imageupload", Parameter: dicParameter as NSDictionary, Images: [dicImgData!])
//            //            }
//            //        }
//         //   let headers = ["Authorization": authTokens]
//            let   body = ["created_user_id":"1", "code": codeTextField.text!, "name": nameTextField.text!, "category":"1", "purchase": productPriceTextField.text!, "retail": retailPriceTextField.text!] as [String:String]
//          //  let image = self.imageCategory.image!
//            ProgressHUD.show()
//            DispatchQueue.global().async {
//
//                Alamofire.upload(multipartFormData:
//                    {
//                        (multipartFormData) in
//                        multipartFormData.append(UIImagePNGRepresentation(image!)!, withName: "uploadFile", fileName: "file.jpeg", mimeType: "image/jpeg")
//                         ProgressHUD.dismiss()
//                        for (key, value) in body
//                        {
//                            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//                        }
//                        //multipartFormData.
//                }, to:url,headers:nil)
//                { (result) in
//                    switch result {
//                    case .success(let upload,_,_ ):
//                        upload.uploadProgress(closure: { (progress) in
//                            print(progress)
//                        })
//                        upload.responseJSON
//                            { response in
//                                print(response.result)
//                                if response.result.value != nil
//                                {
//
//
//                                    let dict :NSDictionary = response.result.value! as! NSDictionary
//                                    print(dict)
//                                   let status = dict.value(forKey: "status")as! Bool
//                                    if status == true
//                                    {
//                                        print("DATA UPLOAD SUCCESSFULLY")
//                                    }
//                                }
//                        }
//                    case .failure(let encodingError):
//                        print("e")
//                        print(encodingError)
//                        break
//                    }
//                }
//            }
//            }
//        }

     func postTimeLine(fileData:Data?) {
        
        
        
        var parameters: Dictionary<String,AnyObject>?
        var postTimeLineURL: String
        var flag = true
        
        if let prod = productId {
          flag = false
            postTimeLineURL = PSCommon.Global.baseURL+"products/update"
            parameters = ["user_id":"1", "code": codeTextField.text!, "name": nameTextField.text!, "category":categoryId!, "purchase": productPriceTextField.text!, "retail": retailPriceTextField.text!, "id":prod, "status":"1"] as [String: AnyObject]
            
            
        }
        else {
           postTimeLineURL = PSCommon.Global.baseURL+"products/insert"
            parameters = ["user_id":"1", "code": codeTextField.text!, "name": nameTextField.text!, "category":categoryId!, "purchase": productPriceTextField.text!, "retail": retailPriceTextField.text!] as [String: AnyObject]

        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }

            if fileData != nil {
                multipartFormData.append(fileData!, withName: "uploadFile", fileName: "user.png", mimeType: "image/png")
            }

          

        }, to: postTimeLineURL, encodingCompletion: { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let upload, _, _):

                    upload.responseJSON { response in
//
//                     //   removeHUD()
//                        //                    self.delegate?.showSuccessAlert()
//                        print(response.request)  // original URL request
//                        print(response.response) // URL response
//                        print(response.data)     // server data
//                        print(response.result)   // result of response serialization
//                        //                        self.showSuccesAlert()
//                        //                    self.removeImage("frame", fileExtension: "txt")
                        if response.result.value != nil {
                            if flag == false {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSProductViewController") as! PSProductViewController
                                SDImageCache.shared().clearMemory()
                                SDImageCache.shared().clearDisk()
                                
                                    self.showDetailViewController(vc, sender: self)
                                
                                
                            }else {
                                let alertController = UIAlertController(title: "Success", message:
                                    "Successfully Added New Product", preferredStyle: UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                                self.present(alertController, animated: true, completion: {
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSAddProductViewController") as! PSAddProductViewController
                                    
                                    self.showDetailViewController(vc, sender: self)
                                })
                            
                            }
                           
                            //return;
                        }
                      
                    }

                case .failure(let encodingError):
                    //                self.delegate?.showFailAlert()
                    print(encodingError)
                   // responseHandler(nil,false)
                }
            }


        })
   }
    
    func getProduct(){
        let url = PSCommon.Global.baseURL+"products/edit"
        var body: Dictionary<String,AnyObject>?
        body = ["id":productId] as [String:AnyObject]
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //print("JSON: \(json)")
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        if let dict = resultDict["message"]  as? NSDictionary {
                           
                            if let code = dict["code"]  {
                                self.codeTextField.text = "\(code)"
                            }
                            if let name = dict["name"]  {
                                self.nameTextField.text = "\(name)"
                            }
                            if let cat = dict["category"]  {
                                self.categoryId = "\(cat)"
                                
                            }
                            if let catName = dict["category_name"]  {
                                self.productCategoryTextField.text = "\(catName)"
                                
                            }
                            if let rprice = dict["price"]  {
                                self.retailPriceTextField.text = "\(rprice)"
                            }
                            if let pprice = dict["cost"]  {
                                self.productPriceTextField.text = "\(pprice)"
                            }
                            if let image = dict["image"] as? String  {
                               
                             //  print(image)
                                
                                let imageURL = NSURL(string: image)
                                //productImage?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
                                self.imageCategory?.sd_setImage(with: imageURL as! URL, placeholderImage: UIImage(named: "placeholder"))
                                                   // imageCategory.sd_setImage(with: <#T##URL?#>, completed: <#T##SDExternalCompletionBlock?##SDExternalCompletionBlock?##(UIImage?, Error?, SDImageCacheType, URL?) -> Void#>)
                            }
                            
                        }
                    }
                    else{
                        let message = json["message"].string
                        // print(message)
                        let alertController = UIAlertController(title: "Error", message:
                            message, preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSProductViewController") as! PSProductViewController
        showDetailViewController(vc, sender: self)
    }
}




