//
//  PSCategoryTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 10/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
   
    var editObject : (() -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func editButtonPressed(_ sender: UIButton) {
        
        if let btnAction = self.editObject
        {
            btnAction()
        }
    }
}
