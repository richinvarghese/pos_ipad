//
//  PSCustomerTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 06/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSCustomerTableViewCell: UITableViewCell {

   
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var mobileLabel: UILabel!
    var historyObject : (() -> Void)? = nil
     var editObject : (() -> Void)? = nil
    
    
    @IBOutlet weak var emailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showHistoryButtonPressed(_ sender: Any) {
        if let btnAction = self.historyObject
        {
            btnAction()
        }
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        if let btnAction = self.editObject
        {
            btnAction()
        }
    }
    
}
