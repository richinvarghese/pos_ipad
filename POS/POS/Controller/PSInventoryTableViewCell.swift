//
//  PSInventoryTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 22/08/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSInventoryTableViewCell: UITableViewCell {

    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
