//
//  PSAddGiftCardViewController.swift
//  POS
//
//  Created by Richin Varghese on 07/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON

class PSAddGiftCardViewController: UIViewController {

    @IBOutlet weak var giftCardNumberTextField: UITextField!
    
    @IBOutlet weak var expiryTextField: UITextField!
    @IBOutlet weak var valueTextField: UITextField!
    
    let expiryDatePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        createDatePicker()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func addGiftCardButtonPressed(_ sender: UIButton) {
        addGiftCard()
        
    }
    @IBAction func gnerateButtonPressed(_ sender: UIButton) {
        let sz: UInt64 = 100000000
        let ld: UInt64 = UInt64(arc4random_uniform(9)+1)
        let ms: UInt64 = UInt64(arc4random_uniform(UInt32(sz/10)))
        let ls: UInt64 = UInt64(arc4random_uniform(UInt32(sz)))
        let digits: UInt64 = ld * (sz*sz/10) + (ms * sz) + ls
        giftCardNumberTextField.text = String(digits)
        
       // print(String(format:"16 digits: %llu", digits))
    }
    
    func addGiftCard(){
        
         let url = PSCommon.Global.baseURL+"gift_card/insert"
        var body: Dictionary<String,AnyObject>?
        
        body = ["created_user_id":"1", "gift_card_numb": giftCardNumberTextField.text!, "value":valueTextField.text!, "expiry_date":expiryTextField.text!] as [String:AnyObject]
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                if json["status"] == "success" {
                   // print(json["message"])
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSGiftCardViewController")
                        as! PSGiftCardViewController
                    self.showDetailViewController(vc, sender: self)
                    
                  /*  let alertController = UIAlertController(title: "Success", message:
                        "Saved", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Success", style: UIAlertActionStyle.default,handler: nil))*/
                    
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func createDatePicker() {
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        expiryTextField.inputAccessoryView = toolbar
        expiryTextField.inputView = expiryDatePicker
        expiryDatePicker.datePickerMode = .date
        
    }
    
    @objc func donePressed(){
        let formatter = DateFormatter()
     //   formatter.dateStyle = .long
       // formatter.timeStyle = .none
        formatter.dateFormat = "dd-M-YYYY"
        let dateString = formatter.string(from: expiryDatePicker.date)
        expiryTextField.text = "\(dateString)"
        self.view.endEditing(true)
    }
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSGiftCardViewController")
            as! PSGiftCardViewController
        showDetailViewController(vc, sender: self)

    }
    
}
