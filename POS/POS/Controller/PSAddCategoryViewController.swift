//
//  PSAddCategoryViewController.swift
//  POS
//
//  Created by Richin Varghese on 10/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON


class PSAddCategoryViewController: UIViewController {

    var categoryId: String?
    var status: String?
    
    @IBOutlet weak var categoryTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if categoryId != nil {
            getCategory()
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        addCategory()
    }
    
    func addCategory(){
        
        var body: Dictionary<String,AnyObject>?
        var url: String
        
        if let catID = categoryId {
            url = PSCommon.Global.baseURL+"category/update"
            body = ["user_id":"1", "id":catID, "category":categoryTextField.text!, "status":"1"] as [String:AnyObject]
        }
        else{
            url = PSCommon.Global.baseURL+"category/insert"
            body = ["created_user_id":"1", "category": categoryTextField.text!] as [String:AnyObject]

        }
        
        
       
        
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
               // print("JSON: \(json)")
                if json["status"] == "success" {
                    // print(json["message"])
                    if self.categoryId != nil {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PSCategoryViewController") as! PSCategoryViewController
                    self.showDetailViewController(vc, sender: self)
                    }
                      let alertController = UIAlertController(title: "Success", message:
                     "Saved", preferredStyle: UIAlertControllerStyle.alert)
                     alertController.addAction(UIAlertAction(title: "Success", style: UIAlertActionStyle.default,handler: nil))
                        self.categoryTextField.text! = ""
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func getCategory(){
        let url = PSCommon.Global.baseURL+"category/edit"
        var body: Dictionary<String,AnyObject>?
        body = ["id":categoryId] as [String:AnyObject]
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        if let dict = resultDict["message"]  as? NSDictionary {
                            
                            if let name = dict["category_name"]  {
                                self.categoryTextField.text = "\(name)"
                            }
                            if let catId = dict["id"]  {
                                self.categoryId = "\(catId)"
                            }
                            if let status = dict["status"]  {
                                self.status = "\(status)"
                            }
                           
                            
                        }
                    }
                    else{
                        let message = json["message"].string
                        // print(message)
                        let alertController = UIAlertController(title: "Error", message:
                            message, preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                    
                    
                else{
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }

    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSCategoryViewController")
            as! PSCategoryViewController
        showDetailViewController(vc, sender: self)
    }
}
