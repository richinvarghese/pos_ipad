//
//  PSShowOrderTableViewCell.swift
//  POS
//
//  Created by Richin Varghese on 09/10/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit

class PSShowOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var slnoLabel: UILabel!
    
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var perItemLabel: UILabel!
    
    @IBOutlet weak var totalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
