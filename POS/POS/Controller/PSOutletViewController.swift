//
//  PSOutletViewController.swift
//  POS
//
//  Created by Richin Varghese on 28/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import SDWebImage
import ProgressHUD
import Alamofire
import SwiftyJSON
import iOSDropDown


class PSOutletViewController:  CommonViewController, UISearchBarDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegate,  UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, StarIoExtManagerDelegate {
    @IBOutlet weak var openCategoryView: UIView!
    @IBOutlet weak var openCategoryTextField: UITextField!
    
    @IBOutlet weak var openCategoryPriceTextField: UITextField!
    @IBOutlet weak var openCategoryCentralLayoutConstraint:
    NSLayoutConstraint!
    var categoryPicker = UIPickerView()
    var orderID : String?
    
    @IBOutlet weak var barcodeView: UIView!
    @IBOutlet weak var searchText: UISearchBar!
     var productList = [NSDictionary]()
   
    @IBOutlet weak var productTextField: UITextField!
    var productPicker = UIPickerView()
    var selectedProductId: String?
    var selectedProductName: String?
    var selectedProductPrice: String?
    var selectedproductCode: String?
    var selectedCategoryName: String?
    
    // The list of array to display. Can be changed dynamically
    @IBOutlet weak var discountTextField: UITextField!
    
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var countItemLabel: UILabel!
    var totalItems = 0.0
    @IBOutlet weak var totalItemLabel: UICollectionView!
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var categorySectionHeader: UICollectionView!
    var categoryArray = [NSDictionary]()
    var filteredArray = [NSDictionary]()
    var tempCategory = [NSDictionary]()
    @IBOutlet weak var categoryCollectionview: UICollectionView!
    var menu = ["Dashboard", "Customers", "Gift Card", "Sales", "Reports", "Point Of Sales", "Inventory", "Products", "System Setting"]
    var cart = [Int : [String: Any]]()
    var quantity:Int?
    var addToCart = [PSProduct]()
    var order = PSOrder()
    var deleteProduct = [PSDeleteProduct]()
    var taxDAata = String()
    var taxFlag = false
    var i = 0
    
     var totalPrice = 0.0
    
    let menuImages: [UIImage] = [
        UIImage(named: "IC_Dashboard")!,
        UIImage(named: "IC_Customer")!,
        UIImage(named: "IC_Gift")!,
        UIImage(named: "IC_Sales")!,
        UIImage(named: "IC_Report")!,
        UIImage(named: "IC_POS")!,
        UIImage(named: "IC_Inventory")!,
        UIImage(named: "IC_Product")!,
        UIImage(named: "IC_Setting")!
    ]
    
    var customer = [NSDictionary]()// ["Sam", "Patric", "Sanju", "Sham"]
   // var customer = [String : String].self

    var customerId = "1"
    @IBOutlet weak var customerTextField: UITextField!
    
        var pickerView = UIPickerView()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView ==  self.pickerView{
        return customer.count
        }
        else if pickerView == self.categoryPicker{
            return categoryArray.count+1
        }
        else  {
            return productList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       // return customer[row]
        if pickerView == self.pickerView {
            if let name = customer[row]["fullname"] as? String  {
                return name
            }
            else {
                return "Nil"
            }
            }
        else if pickerView == self.categoryPicker {
            if(row==0){
                return "Select Item"
            }else{
            if let name =  categoryArray[row-1]["name"] as? String  {
                return name
            }
            
            else {
                return "Nil"
            }
            }
        }
        else {
            if let pro = productList[row]["name"] as? String {
                return pro
            }
            else {
                return "Nil"
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.pickerView {
        customerTextField.text =  customer[row]["fullname"] as! String
        order.customerId = customer[row]["id"]! as! String
        //print(order.customerId)
        order.customer = customerTextField.text!
        }
        else if pickerView == self.categoryPicker {
            if row == 0 {
                 openCategoryTextField.text = "Select Item"
            }else {
            openCategoryTextField.text = categoryArray[row-1]["name"] as! String
            }
        }
        else {
            if let id =  productList[row]["id"]  {
                
            
            productTextField.text = productList[row]["name"] as! String
            selectedProductId = productList[row]["id"] as! String
            selectedproductCode = productList[row]["code"] as! String
            selectedProductName = productList[row]["name"] as! String
            selectedProductPrice = productList[row]["retail_price"] as! String
            selectedCategoryName = productList[row]["catname"] as! String
            }
            //print(selectedproductCode)
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
       openCategoryCentralLayoutConstraint.constant = -900
        openCategoryView.layer.cornerRadius = 10
        openCategoryView.layer.masksToBounds = true
        // The list of array to display. Can be changed dynamically
       // let  barcodeDropdown = DropDown(frame: CGRect(x: 110, y: 140, width: 200, height: 30)) // set frame

       
        quantity = 0
        pickerView.delegate = self
        pickerView.dataSource = self
        productPicker.dataSource = self
        productPicker.delegate = self
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
        categoryCollectionview.dataSource = self
        categoryCollectionview.delegate = self
        //productTextField.inputView = productPicker
        customerTextField.inputView = pickerView
        openCategoryTextField.inputView = categoryPicker
        customerTextField.textAlignment = .center
        discountTextField.delegate = self
       
        var layout  =  self.categoryCollectionview.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 40, right: 10)
        layout.minimumInteritemSpacing = 10
       
        
        layout.itemSize = CGSize(width: (self.categoryCollectionview.frame.size.width) / 4, height: self.categoryCollectionview.frame.size.height / 4)
        
        viewCategory()
        
       // UISplitViewController.d
        self.splitViewController?.preferredDisplayMode = .primaryHidden
        customerTextField.text = "Walk In Customer"
       searchText.delegate = self
        
        if let ord = orderID {
            //print("orderid"+ord)
            fillOrder(orderId: ord)
        }
        
  
        
    }
    
    /*override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     
        
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func backButtonPressed(_ sender: UIButton) {
      //  navigationController?.popViewController(animated: true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSDashboardViewController") as! PSDashboardViewController
        showDetailViewController(vc, sender: self)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       // print(categoryArray[section]["product_count"]!)
        return categoryArray[section]["product_count"] as! Int
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoryCollectionview.dequeueReusableCell(withReuseIdentifier: "PSCategoryCollectionViewCell", for: indexPath) as! PSCategoryCollectionViewCell
       
        if let product = categoryArray[indexPath.section]["product"] as? [NSDictionary] {
            let productImage = URL(string: product[indexPath.item]["image"] as! String)
            
            cell.productImageView.sd_setImage(with: productImage, placeholderImage: UIImage(named: "placeholder"))
            cell.poductNameLabel.text = product[indexPath.item]["name"] as? String
        }
        
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = categoryCollectionview.cellForItem(at: indexPath)
        //     cell?.layer.borderColor = UIColor(red: 247/255.0, green: 49/255.0, blue: 72/255.0, alpha: 1).cgColor
        let color1 = UIColor(hexString: "#F73148")
        cell?.layer.borderColor = color1.cgColor
        cell?.layer.borderWidth = 2
        
         var flag = false
        if let product = categoryArray[indexPath.section]["product"] as? [NSDictionary] {
            var j = 0
            for add in addToCart {
                if add.productCode ==  product[indexPath.item]["pcode"] as? String{
                    flag = true;
                    if let q = add.qty {
                        addToCart[j].qty = q + 1
                        addToCart[j].subTotal = Double(addToCart[j].qty!) * Double(addToCart[j].price!)!
                        //add.qty = q + 1
                      //  print(addToCart[j].qty)
                       
                    }
                }
                j += 1
            }
            
            if flag == false {
            let item  = PSProduct()
            item.categoryCode = categoryArray[indexPath.section]["name"] as? String
            item.price = product[indexPath.item]["retail_price"] as? String
            item.productName =  product[indexPath.item]["name"] as? String
            item.productCode = product[indexPath.item]["pcode"] as? String
               // print(item.categoryCode)
            item.qty = 1
                item.subTotal = Double(item.qty!) * Double(item.price!)!
               //print(product[indexPath.item]["qty"]!)
                if let qty = product[indexPath.item]["qty"] as? String {
                    print(qty)
                    if (Int(qty)! <= 0)  {
                        let alertController = UIAlertController(title: "Notification", message:
                            "Inventory is zero.Please Update", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                        

                    else{
                        //print("ee")
                    }
                    
                }
            addToCart.append(item)
            }
          
          
        }
        //print(cart)
        cartTableView.reloadData()
        
       /* let item = categoryCollectionview.dequeueReusableCell(withReuseIdentifier: "PSCategoryCollectionViewCell", for: indexPath) as! PSCategoryCollectionViewCell
        print(item.poductNameLabel!.text!)*/
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = categoryCollectionview.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.darkGray.cgColor
        cell?.layer.borderWidth = 0.5
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
      //  categoryArray
        return categoryArray.count
    }
    
    func viewCategory(){
        let url = PSCommon.Global.baseURL+"pos/view"
            //PSCommon.Global.baseURL+"pos/view"
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                    if let dict = resultDict["message"]  as? NSDictionary {
                            if let arrayDict  = dict["category"] as? [NSDictionary]{
                                self.categoryArray = arrayDict
                                self.tempCategory = arrayDict
                                self.categoryCollectionview.reloadData()
                            }
                        if let tax = dict["tax"] as? String {
                            self.taxDAata = "\(tax)"
                            self.taxLabel.text = "0"// self.taxDAata
                        }
                        if let customer = dict["customer"] as? [NSDictionary] {
                            self.customer = customer
                            // print(customer)
                            self.customerTextField.reloadInputViews()
                           
                            
                           
                        }
                        if let product = dict["product"] as? [NSDictionary] {
                            self.productList = product
                            self.productTextField.reloadInputViews()
                        }
                        
                        }
                    }
                    else{
                        let message = json["message"].string
                       // print(message)
                        let alertController = UIAlertController(title: "Error", message:
                            message, preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                else{
                   
                    let message = json["message"].string
                 
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeaderView = categoryCollectionview.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PSCategoryCollectionReusableView" , for: indexPath) as! PSCategoryCollectionReusableView
        if (categoryArray[indexPath.section]["product_count"]!as! Int) != 0 {
             sectionHeaderView.isHidden = false
      //  if categoryArray[indexPath.section][indexPath.row].count != 0{
        sectionHeaderView.categoryTitleLabel.text = (categoryArray[indexPath.section]["name"] as! String)
        }else{
            sectionHeaderView.isHidden = true
        }
        return sectionHeaderView
        
        }
    
    @IBAction func taxswitch(_ sender: UISwitch) {
        if sender.isOn == true {
         // print("true")
            self.taxLabel.text = taxDAata
            taxFlag = true
            order.tax = Double(taxDAata)
            order.taxTotal = (order.subTotal!*order.tax!)/100.0 //order.grandTotal! - (order.discountAmt!+order.subTotal!)
            
          //  taxLabel.isHidden = false
        }else{
            self.taxLabel.text = "0"
            order.tax = 0
            order.taxTotal = 0
            taxFlag = false
           

           
        }
        let total = order.subTotal! - order.discountAmt!
        grandTotal.text = "\(total + total * order.tax!/100)"
        order.grandTotal = total + total * order.tax!/100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var sum = 0
        var qty = 0
        var subTotal = 0.0
        var price = 0.0
        var discountIndividual = 0.0
       
        
        
        for item in addToCart {
            
            //sum += Double(item.price)! * Double(item.qty)
            if let q = item.qty {
                sum += q
                qty = q
            }
            if let p = item.price {
                price = (p as NSString).doubleValue
            }
            if let d = item.discount {
                discountIndividual = d//(d as NSString).doubleValue
            }
            subTotal += (Double(qty) * price) - discountIndividual
            
           
        }
        var tax = 0.0
        if let t = taxLabel.text {
            tax = (t as NSString).doubleValue
            order.tax = tax
        }else{
            order.tax = 0.0
        }
        var discount = 0.0
        if let dis = discountTextField.text {
            discount = (dis as NSString).doubleValue
            order.discountAmt = discount
        }
        else{
            order.discountAmt = 0.0
        }
        
        totalPrice = subTotal + (subTotal*tax)/100.0
        order.taxTotal = totalPrice - (subTotal + order.discountAmt!)
        totalPrice = totalPrice - discount
        countItemLabel.text = "\(sum)"
        subTotalLabel.text = "\(subTotal)"
        order.subTotal = subTotal
        grandTotal.text = "\(totalPrice)"
        
        order.grandTotal = totalPrice
        order.qty = qty
      //  order.calculate()
        return  addToCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = cartTableView.dequeueReusableCell(withIdentifier: "PSCartTableViewCell") as! PSCartTableViewCell
        
        
         cell.itemNameLabel!.text = addToCart[indexPath.row].productName
         cell.categoryLabel!.text = addToCart[indexPath.row].categoryName
        cell.priceLabel!.text = addToCart[indexPath.row].price
        //cell.qtyTextField.text = "\(self.quantity!)"
        cell.addobj =
            {
                 self.addToCart[indexPath.row].qty =   self.addToCart[indexPath.row].qty! + 1
                cell.qtyTextField.text = "\(self.addToCart[indexPath.row].qty!)"
                
                self.cartTableView.reloadData()
        }
        cell.minusobj =
            {
            if self.addToCart[indexPath.row].qty! > 1
                {
                    self.addToCart[indexPath.row].qty = self.addToCart[indexPath.row].qty! - 1
                    cell.qtyTextField.text = "\(self.addToCart[indexPath.row].qty!)"
                    
                    self.cartTableView.reloadData()
                }
               
        }
        let q = addToCart[indexPath.row].qty
         let qt = String(q!)
        cell.qtyTextField!.text =  qt
    
        var purchase_price  = 0.0
        if let price  =  cell.priceLabel.text {
            purchase_price = Double(price)!
            //print(purchase_price);
        }
        var quantity  = 0.0
        if let qty  = cell.qtyTextField.text{
          quantity = Double(qty)!
            totalItems += quantity
           // print(quantity);
        }
        
       
       
        let subtotal = purchase_price * quantity
        cell.subtotalPriceLabel.text = String(subtotal)
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
           
           // self.cart.remove(at: cart.values.i)
            
          //  self.menu.remove(at: indexPath.row)
            
            if order.salesID != "" {
                var item  = PSDeleteProduct()
                item.categoryCode = addToCart[indexPath.row].categoryCode
                item.categoryName = addToCart[indexPath.row].categoryName
                item.discount = addToCart[indexPath.row].discount
                item.id = addToCart[indexPath.row].id
                item.price = addToCart[indexPath.row].price
                item.productCode = addToCart[indexPath.row].productCode
                item.productName = addToCart[indexPath.row].productName
                item.qty = addToCart[indexPath.row].qty
                item.subTotal = addToCart[indexPath.row].subTotal
                deleteProduct.append(item)
                
                //print(addToCart[indexPath.row].productCode)
            }
            
            addToCart.remove(at: indexPath.row)
            
            self.cartTableView.deleteRows(at: [indexPath], with: .automatic)
            
           
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //  let pcode =  addToCart[indexPath.row].productCode
        
        let alertController = UIAlertController(title: "Discount Amount", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            let disc = self.addToCart[indexPath.row].discount
            textField.keyboardType = UIKeyboardType.numberPad
            textField.placeholder = "Enter discount amount for this product"
            textField.text = ""//"\(disc!)"
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0].text
            //print(firstTextField)
            self.addToCart[indexPath.row].discount = Double(firstTextField!)
            self.calculate()
            
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in })
     
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)

    }
    
    func insert(productCode:String){
        menu.append(productCode)
    }

    @IBAction func qtyTextFieldChanged(_ sender: UITextField) {
    
    }
    
    @IBAction func discountChanged(_ sender: UITextField) {
        
        /*var discount =  order.discountAmt
        var grand = 0.0
        if let dis = discountTextField.text {
            discount = (dis as NSString).doubleValue
        }*/
        if  discountTextField.text != nil {
        if subTotalLabel.text != nil {
            //grand =  totalPrice
            //grand = grand - discount
            order.discountAmt = Double(discountTextField.text!)
            grandTotal.text =  "\(order.subTotal! - order.discountAmt! )"
            order.grandTotal = Double(grandTotal.text!)
        }
        
        }
    }
    @IBAction func paymentAction(_ sender: UIButton) {
        
        
        if(order.salesID == "") {
        
            showPopupController()
        
       
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "PSInvoiceViewController") as! PSInvoiceViewController
        vc.addToCart = self.addToCart
        navigationController?.show(vc, sender: self)*/
        }
        else {
            let alert = UIAlertController(title: "Choose Your Action", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Void Transaction", style: .default, handler: { _ in
                self.voidPayment()
            }))
            
            alert.addAction(UIAlertAction(title: "Return/Exchange", style: .default, handler: { _ in
                self.showPopupController()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender
                alert.popoverPresentationController?.sourceRect = sender.bounds
                alert.popoverPresentationController?.permittedArrowDirections = .down
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
            
          
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == discountTextField {
            discountTextField.resignFirstResponder()
        }
        customerTextField.resignFirstResponder()
        return true
    }
    
    @IBAction func cancel(_ sender: UIButton) {
         let vc = storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
        showDetailViewController(vc, sender: self)
    }
    
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
       /* let vc = storyboard?.instantiateViewController(withIdentifier: "PSLoginViewController") as! PSLoginViewController
        //self.navigationController?.popViewController(animated: true);
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    @IBAction func todaysSaleButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PSTodaySalesViewController")
            as! PSTodaySalesViewController
        showDetailViewController(vc, sender: self)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        categoryArray = (searchText.isEmpty ? tempCategory : categoryArray.filter({(dataString: NSDictionary) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return (dataString["name"] as! String).range(of: searchText, options: .caseInsensitive) != nil
        }))
        
        self.categoryCollectionview.reloadData()
    }
    
   
    @IBAction func productTextFieldChanged(_ sender: UITextField) {
        if let pcode = productTextField.text {
            var flag = false
            
            var j = 0
            for add in addToCart {
               // print(add.productCode!+"=="+selectedproductCode! )
                
                if add.productCode ==  pcode {
                    flag = true;
                    if let q = add.qty {
                        addToCart[j].qty = q + 1
                        addToCart[j].subTotal = Double(addToCart[j].qty!) * Double(addToCart[j].price!)!
                        
                    }
                    productTextField.text = ""
                    break
                }
                j += 1
            }
            
            if flag == false {
            for i in 1..<productList.count {
                //print(productList[i])
                if let productCode = productList[i]["code"] {
                    if productCode as! String == pcode {
                        //print(productList[i]["name"])
                        print(productList[i]["catname"] as! String)
                        let item  = PSProduct()
                        item.categoryCode = productList[i]["catname"] as! String
                        item.price = productList[i]["retail_price"] as! String
                        item.productName = productList[i]["name"] as! String
                        item.productCode = productList[i]["code"] as! String
                        item.qty = 1
                        item.subTotal =  Double(item.qty!) * Double(item.price!)!
                        addToCart.append(item)
                        productTextField.text = ""
                        break
                    }
                }
               
                    
                }
            }
            cartTableView.reloadData()
            
            }
/*    catname = sample;
 code = 10101;
 "created_datetime" = "2018-08-01 14:20:42";
 "created_user_id" = 1;
 id = 6;
 name = hi;
 "outlet_id" = "<null>";
 "purchase_price" = "100.00";
 qty = "<null>";
 "retail_price" = "0.00";
 status = 1;
 thumbnail = "10101.png";
 "updated_datetime" = "0000-00-00 00:00:00";
 "updated_user_id" = 0;
*/
     
    }
    @IBAction func addProductButtonPressed(_ sender: UIButton) {
        if selectedProductId != nil{
        var flag = false
        
            var j = 0
            for add in addToCart {
               // print(add.productCode!+"=="+selectedproductCode! )
                
                if add.productCode ==  selectedproductCode {
                    flag = true;
                    if let q = add.qty {
                        addToCart[j].qty = q + 1
                        addToCart[j].subTotal = Double(addToCart[j].qty!) * Double(addToCart[j].price!)!
                       
                    }
                }
                j += 1
            }
            
            if flag == false {
                for p in productList {
                    if p["pcode"] as! String == selectedproductCode {
                        let item  = PSProduct()
                        item.categoryCode = p["catname"] as! String/// check
                        item.price = p["retail_price"] as! String
                        item.productName = p["name"] as! String
                        item.productCode = p["code"] as! String
                        item.qty = 1
                        item.subTotal =  Double(item.qty!) * Double(item.price!)!
                       /* productTextField.text = productList[row]["name"] as! String
                        selectedProductId = productList[row]["id"] as! String
                        selectedproductCode = productList[row]["code"] as! String
                        selectedProductName = productList[row]["name"] as! String
                        selectedProductPrice = productList[row]["purchase_price"] as! String
                        selectedCategoryName = productList[row]["catname"] as! String
                       */
                        addToCart.append(item)
                    
                    
                    }
                }
                
            }
            
            
       
        //print(cart)
        cartTableView.reloadData()
        productTextField.text = ""
            selectedProductId = nil
           productPicker.selectRow(0, inComponent: 0, animated: true)
        }
    }
    
    @IBAction func openCashDrawer(_ sender: UIButton) {
        let commands: Data
        commands = CashDrawerFunctions.createData(AppDelegate.getEmulation(), channel: SCBPeripheralChannel.no1)
        
        let portName:     String = AppDelegate.getPortName()
        let portSettings: String = AppDelegate.getPortSettings()
        let timeout:      UInt32 = 10000                             // 10000mS!!!
        
        self.blind = true
        
        GlobalQueueManager.shared.serialQueue.async {
          
           
                 Communication.sendCommands(commands,
                                               portName: portName,
                                               portSettings: portSettings,
                                               timeout: timeout,
                                               completionHandler: { (result: Bool, title: String, message: String) in
                                                DispatchQueue.main.async {
                                                    let alertView: UIAlertView = UIAlertView(title: title,
                                                                                             message: message,
                                                                                             delegate: nil,
                                                                                             cancelButtonTitle: "OK")
                                                    
                                                    alertView.show()
                                                    
                                                    self.blind = false
                                                }
                })
            
            // case 1, 3 :
            /*default   :
             _ = Communication.sendCommandsDoNotCheckCondition(commands,
             portName: portName,
             portSettings: portSettings,
             timeout: timeout,
             completionHandler: { (result: Bool, title: String, message: String) in
             DispatchQueue.main.async {
             let alertView: UIAlertView = UIAlertView(title: title,
             message: message,
             delegate: nil,
             cancelButtonTitle: "OK")
             
             alertView.show()
             
             self.blind = false
             }
             
             })*/
           
            }
        }
    
    func calculate(){
        var sum = 0
        var qty = 0
        var subTotal = 0.0
        var price = 0.0
        var discountIndividual = 0.0
        
        
       
        for item in addToCart {
            
            //sum += Double(item.price)! * Double(item.qty)
            if let q = item.qty {
                sum += q
                qty = q
            }
            if let p = item.price {
                price = (p as NSString).doubleValue
            }
            
            if let d = item.discount {
                print("dis = \(d)")
                discountIndividual += d//(d as NSString).doubleValue
            }
            subTotal += (Double(qty) * price) //- discountIndividual
            
            
        }
        discountTextField.text = "\(discountIndividual)"
        var tax = 0.0
        if let t = taxLabel.text {
            tax = (t as NSString).doubleValue
            order.tax = tax
        }else{
            order.tax = 0.0
        }
        var discount = 0.0
        if let dis = discountTextField.text {
            discount = (dis as NSString).doubleValue
            order.discountAmt = discount
        }
        else{
            order.discountAmt = 0.0
        }
        
        totalPrice = subTotal + (subTotal*tax)/100.0
        order.taxTotal =  (subTotal*tax)/100.0//totalPrice - (subTotal + order.discountAmt!)
        totalPrice = totalPrice - discount
        countItemLabel.text = "\(sum)"
        subTotalLabel.text = "\(subTotal)"
        order.subTotal = subTotal
        grandTotal.text = "\(totalPrice)"
        
        order.grandTotal = totalPrice
        order.qty = qty
    }
    
    func fillOrder(orderId: String)
    {
        let url = PSCommon.Global.baseURL+"report/invoice"
        ProgressHUD.show()
        var body: Dictionary<String,AnyObject>?
        
        body = ["order_id":orderId] as [String:AnyObject]
        
        //   body = ["outlet":"1", "paid": "0", "start_date":startTextField.text!, "end_date":endTextField.text!] as [String:AnyObject]
        
       
        Alamofire.request(url, method: .post, parameters: body, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                // print(json)
                
                if let resultDict = response.result.value as? NSDictionary {
                    
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"]  as? NSDictionary {
                            if let orderDict  = dict["order_details"] as? [NSDictionary]{
                                //print(orderDict)
                                for p in orderDict {
                                    
                                        let item  = PSProduct()
                                    
                                    
                                    if let dis = (p["discount"] as? NSString)?.doubleValue  {
                                     //   item.discount = dis
                                       // print(dis)
                                    }
                                   
                                    if let name = p["name"] {
                                        item.productName = name as? String
                                    }
                                    if let pcode = p["pcode"] {
                                        item.productCode = pcode as? String
                                    }
                                    if let price = p["price"] {
                                        item.price = price as? String
                                    }
                                    if let qty = (p["qty"] as? NSString)?.integerValue {
                                        item.qty = qty
                                        let pr = (item.price! as NSString).doubleValue
                                        item.subTotal =  pr*Double(item.qty!)
                                    }
                                    if let stotal = (p["subtotal"] as? NSString)?.doubleValue {
                                    //    item.subTotal = stotal
                                        
                                    }
                                   
                                    self.addToCart.append(item)
                                   
                                }
                                // self.calculate()
                                 self.cartTableView.reloadData()
                               
                             
                            }
                            
                            if let sum = dict["order"] as? NSDictionary{
                               
                                if let cname  = sum["cust_fullname"] {
                                    self.order.customer = cname as! String
                                }
                               
                               // (p["qty"] as? NSString)?.integerValue
                                if let gtotal  = (sum["grandTotal"] as? NSString)?.doubleValue {
                                    self.order.grandTotal = gtotal
                                    self.order.tempGrandTotal = gtotal
                                    
                                }
                                if let paid  = sum["paid_amt"] {
                                    self.order.paidAmount = paid as? String
                                }
                                if let subTotal  = (sum["subTotal"] as? NSString)?.doubleValue {
                                    self.order.subTotal = subTotal
                                }
                                if let dis  = (sum["dis_amt"]  as? NSString)?.doubleValue {
                                    self.order.discountAmt = dis
                                    //print("adasd" + "\(self.order.discountAmt)")
                                    self.discountTextField.text = "\(self.order.discountAmt!)"
                                   // self.order.grandTotal = self.order.grandTotal! + self.order.discountAmt!
                                    //self.grandTotal.text =  "\(self.order.grandTotal!)"
                                }
                               // if let tempGrand =
                            }
                            
                            
                            
                           self.order.salesID = orderId
                            //self.rePrint()
                            
                        }
                    }
                }else{
                    
                    
                    let message = json["message"].string
                    
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
        
       // order.grandTotal = Double(grandTotal.text!)
        
    }
    
    func showPopupController(){
        let popOverVC = storyboard?.instantiateViewController(withIdentifier: "PSPopUpViewController") as! PSPopUpViewController
        popOverVC.addToCart = self.addToCart
        popOverVC.order = self.order
        popOverVC.deleteProduct = self.deleteProduct
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    func voidPayment()
    {
        order.voidStatus = true
        showPopupController()
    }
    
    @IBAction func openCategoryButtonPressed(_ sender: UIButton) {
        openCategoryCentralLayoutConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func openCategoryAddCartButtonPressed(_ sender: UIButton) {
        
        
        var flag = false
        if let product = openCategoryTextField.text {
            var j = 0
            for add in addToCart {
                if add.productCode ==  product{
                    flag = true;
                    if let q = add.qty {
                        addToCart[j].qty = q + 1
                        addToCart[j].subTotal = Double(addToCart[j].qty!) * Double(addToCart[j].price!)!
                        //add.qty = q + 1
                        //  print(addToCart[j].qty)
                        
                    }
                }
                j += 1
            }
            
            if flag == false {
                let item  = PSProduct()
                item.categoryCode = openCategoryTextField.text
                item.price = openCategoryPriceTextField.text
                item.productName =   openCategoryTextField.text
                item.productCode =  openCategoryTextField.text
                // print(item.categoryCode)
                item.qty = 1
                item.subTotal = Double(item.qty!) * Double(item.price!)!
                //print(product[indexPath.item]["qty"]!)
             
                addToCart.append(item)
            }
            
            
        }
        cartTableView.reloadData()
        
        
        openCategoryCentralLayoutConstraint.constant = -900
        UIView.animate(withDuration: 0.3, animations : {  self.view.layoutIfNeeded() })
        
    }
    
    @IBAction func oepnCategoryCancelButtonPressed(_ sender: UIButton) {
        openCategoryCentralLayoutConstraint.constant = -900
        UIView.animate(withDuration: 0.3, animations : {  self.view.layoutIfNeeded() })
        
    }
}


