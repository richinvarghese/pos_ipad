//
//  PSTodaySalesViewController.swift
//  POS
//
//  Created by Richin Varghese on 22/07/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import ProgressHUD
import Alamofire
import SwiftyJSON


class PSTodaySalesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var todaySale = [NSDictionary]()
    
    @IBOutlet weak var salesTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.splitViewController?.preferredDisplayMode = .automatic

        viewSale()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todaySale.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = salesTableView.dequeueReusableCell(withIdentifier: "PSTodaysSalesTableViewCell") as! PSTodaysSalesTableViewCell
        if let dtm = todaySale[indexPath.row]["ordered_dtm"] as? String{
            cell.dateLabel!.text = dtm
        }
        if let salesId = todaySale[indexPath.row]["order_id"] as? String{
            cell.salesIDLabel!.text = salesId
        }
        if let type = todaySale[indexPath.row]["order_type"] as? String{
            cell.typeLabel!.text = type
        }
        if let customerName = todaySale[indexPath.row]["cust_fn"] as? String{
            cell.customerLabel!.text = customerName
        }
        if let items = todaySale[indexPath.row]["total_items"] as? String{
            cell.itemLabel!.text = items
        }
        if let sub = todaySale[indexPath.row]["subTotal"] as? String{
            cell.subTotalLabel!.text = sub
        }
        if let tax = todaySale[indexPath.row]["taxTotal"] as? String{
            cell.taxLabel!.text = tax
        }
        if let grand = todaySale[indexPath.row]["grandTotal"] as? String{
            cell.grandTotalLabel!.text = grand
        }
        
        return cell
    }
    
    func viewSale(){
        
        let url = PSCommon.Global.baseURL+"sales/list"
        
        ProgressHUD.show()
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil) .responseJSON { response in
            ProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                let json = JSON(value)
              //  print("JSON: \(json)")
                // print(response.result.value)
                
                
                if let resultDict = response.result.value as? NSDictionary {
                    if resultDict["status"] as! String  == "success" {
                        
                        if let dict = resultDict["message"] as? [NSDictionary] {
                            self.todaySale = dict
                           
                            self.salesTableView.reloadData()
                        }
                    }
                }else{
                    let message = json["message"].string
                    //print(message)
                    let alertController = UIAlertController(title: "Error", message:
                        message, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
                
            case .failure(let error):
                //your failure code
                print(error)
            }
        }
        
    }
    

}
