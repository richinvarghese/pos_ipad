//
//  PSMasterTableViewController.swift
//  POS
//
//  Created by Richin Varghese on 19/06/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit



class PSMasterTableViewController: UITableViewController {

    let menu = ["Dashboard", "Customers", "Gift Card", "Category", "Todays Sale", "Point Of Sales", "Sales Report", "Transaction List", "Void/Exchange List", "Products", "Inventory", "Settings"]
    
    let menuImages: [UIImage] = [
        UIImage(named: "IC_Dashboard")!,
        UIImage(named: "IC_Customer")!,
        UIImage(named: "IC_Gift")!,
        UIImage(named: "category")!,
        UIImage(named: "IC_Todays_sale")!,
        UIImage(named: "IC_POS")!,
        UIImage(named: "sales_report")!,
        UIImage(named: "sales_report")!,
        UIImage(named: "sales_report")!,
        UIImage(named: "IC_Product")!,
        UIImage(named: "inventory")!,
        UIImage(named: "IC_Setting")!
    ]
    
    var detailVieeController: PSDetailViewController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        let dashboard =  storyboard?.instantiateViewController(withIdentifier: "PSDashboardViewController") as! PSDashboardViewController
        splitViewController?.showDetailViewController(dashboard, sender: self)
        
           // splitViewController?.preferredDisplayMode = .primaryOverlay
  
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menu.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PSMasterTableViewController", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = menu[indexPath.row]
        cell.imageView?.image = menuImages[indexPath.row]

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     // self.splitViewController?.preferredDisplayMode = .allVisible
        switch indexPath.row {
        case 0:
           let dashboard =  storyboard?.instantiateViewController(withIdentifier: "PSDashboardViewController") as! PSDashboardViewController
            splitViewController?.showDetailViewController(dashboard, sender: self)
            
           
            
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSCustomerViewController")
                as! PSCustomerViewController
            showDetailViewController(vc, sender: self)
            
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSGiftCardViewController")
                as! PSGiftCardViewController
            showDetailViewController(vc, sender: self)
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSCategoryViewController")
                as! PSCategoryViewController
            showDetailViewController(vc, sender: self)
        case 4:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSTodaySalesViewController")
                as! PSTodaySalesViewController
            showDetailViewController(vc, sender: self)
        case 5:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSOutletViewController") as! PSOutletViewController
            showDetailViewController(vc, sender: self)
        case 6:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSReportViewController") as! PSReportViewController
            showDetailViewController(vc, sender: self)
        case 7:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSTransactionViewController") as! PSTransactionViewController
            showDetailViewController(vc, sender: self)
        case 8:
            let vc = storyboard?.instantiateViewController(withIdentifier: "VoidExchangeViewController") as! VoidExchangeViewController
            showDetailViewController(vc, sender: self)

        case 9:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSProductViewController") as! PSProductViewController
            showDetailViewController(vc, sender: self)
        case 10:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSInventoryViewController") as! PSInventoryViewController
            showDetailViewController(vc, sender: self)
        case 11:
            //let vc = storyboard?.instantiateViewController(withIdentifier: "PSPaymentViewController") as! PSPaymentViewController
            //showDetailViewController(vc, sender: self)
            let vc = storyboard?.instantiateViewController(withIdentifier: "PSSettingsTableViewController") as! PSSettingsTableViewController
            showDetailViewController(vc, sender: self)
            
        default:
            print("Under Testing")
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    override func addChildViewController(_ childController: UIViewController) {
        view.addSubview(childController.view)
        
    }


}
