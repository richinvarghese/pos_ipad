//
//  POS-Bridging-Header.h
//  POS
//
//  Created by Richin Varghese on 12/08/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

#ifndef POS_Bridging_Header_h
#define POS_Bridging_Header_h

#import <StarIO_Extension/StarIoExt.h>
#import <StarIO_Extension/StarIoExtManager.h>
#import <StarIO_Extension/SMBluetoothManagerFactory.h>

#import <SMCloudServices/SMCloudServices.h>
#import <SMCloudServices/SMCSAllReceipts.h>

#endif /* POS_Bridging_Header_h */
