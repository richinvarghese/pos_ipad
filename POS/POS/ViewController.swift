//
//  ViewController.swift
//  POS
//
//  Created by Richin Varghese on 26/05/18.
//  Copyright © 2018 Richin Varghese. All rights reserved.
//

import UIKit
import iOSDropDown

class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var barcodeDown: DropDown!
    
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var barcodeValue: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.bringSubview(toFront: dropView)
        // Do any additional setup after loading the view, typically from a nib.
        
        barcodeDown.optionArray = ["Option 1", "Option 2", "Option 3"]
        //Its Id Values and its optional
        barcodeDown.optionIds = [1,23,54,22]
        //barcodeDropdown.rowBackgroundColor=.
        // The the Closure returns Selected Index and String
        barcodeDown.didSelect{(selectedText , index ,id) in
            self.barcodeValue.text = "Selected String: \(selectedText) \n index: \(index)"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}

